/* ***** BEGIN LICENSE BLOCK *****
*
* $Id$
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _UTIL_HPP_
#define _UTIL_HPP_

#include <common/dirac.h>

namespace dirac
{
  
  /*  help check the state of the passed object. It requires T to have a
      method called checkValidity with signature: void (*) () const  
      By having checkValidity invoked in both constructor and destructor
      we can ensure that the state of an object is checked going into and before
      coming out of a public method   */
  template < typename T >
  struct ValidityChecker
  {
    ValidityChecker ( T const& obj ) 
      : object_ (obj) 
    {
      object_.checkValidity();
    }
    ~ValidityChecker()
    {
      object_.checkValidity();
    }
  private:
    T const& object_;
  };
  
  /*  reads 4 bytes from a buffer, big endian   */
  uint32
  readFourBytes ( uint8 const* cursor );
  
  template <class T, T val>
  struct constant_value_type
  {
    typedef T                     value_type;
    static const T value = val;
  };
  
  typedef constant_value_type<bool, true> true_type;
  typedef constant_value_type<bool, false> false_type;
    
  //! base class used to prevent user-defined objects from allwoing compiler
  //! generated methods (copy construction and assignment)
  //! inherit privately to use
  class NonCopyable
  {
    typedef NonCopyable                   class_type;
  
  protected:
    NonCopyable();
    ~NonCopyable();
    
    // compiler generated methods are not allowed
    NonCopyable ( class_type const& );
    class_type& operator= ( class_type const& );
  };
  
  
  
  
} /* dirac */




#endif /* _UTIL_HPP_ */
