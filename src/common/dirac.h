/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: dirac.h 67 2008-03-04 13:16:25Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _DIRAC_H_
# define _DIRAC_H_

/*  Common facilities */

#include <config.h>

#ifdef HAVE_STDINT_H

#include <stdint.h>

typedef int8_t                  int8;
typedef uint8_t                 uint8;

typedef int16_t                 int16;
typedef uint16_t                uint16;

typedef int32_t                 int32;
typedef uint32_t                uint32;

typedef int64_t                 int64;
typedef uint64_t                uint64;

#else

typedef signed char             int8;
typedef unsigned char           uint8;

typedef signed short int        int16;
typedef unsigned short int      uint16;

typedef signed int              int32;
typedef unsigned int            uint32;

  #ifdef __GNUC__
  typedef signed long long      int64;
  typedef unsigned long long    uint64;
  #else
    #error "don't know how to represent 64 bit words"
  #endif

#endif


/*  define assert and static asserts for the entire Dirac codebase */
#ifdef __cplusplus
# include <cassert>
#else /* C ? */
# include <assert.h>
#endif // ifdef __cplusplus
#define DIRAC_ASSERT(ex)              assert(ex)
#define DIRAC_MESSAGE_ASSERT(msg, ex) DIRAC_ASSERT((msg && ex))
#define DIRAC_STATIC_ASSERT(ex)       do { typedef int ai[(ex) ? 1 : -1]; } while(0)

/*  we want to apply this macro to arrays only, not classes with operator[]  */
#define DIRAC_ARRAY_SIZE(ar)          ( sizeof ( ar ) / sizeof ( 0[ar] ) )

#endif /* _DIRAC_H_ */
