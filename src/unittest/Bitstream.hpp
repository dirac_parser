/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: Bitstream.hpp 48 2008-02-12 22:48:00Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _BITSTREAM_HPP_
#define _BITSTREAM_HPP_

#include <common/dirac.h>

#include <fstream>
#include <string>
#include <string.h>

class Bitstream
{
#define BUFFER_SIZE   10000
public:
  typedef uint8                           value_type;
  typedef uint32                          size_type;
  typedef uint8*                          iterator;
  typedef uint8 const*                    const_iterator;
  typedef uint8*                          pointer;
  typedef uint8 const*                    const_pointer;
  typedef uint8 const*&                   const_reference;
  
  Bitstream ( std::string filename )  
  : begin_ ( new uint8[BUFFER_SIZE] ), end_ ( 0 ), cursor_ ( 0 )
  , input_ ( filename.c_str() )
  { 
    filePointer_ = 0;
    cursor_ = begin_;
    end_ = begin_;
    addData();
  }
  ~Bitstream() { checkValidity(); delete [] begin_; input_.close(); }
private:
  Bitstream ( Bitstream const& );
  Bitstream const& operator= ( Bitstream const& );
  
public:
  const_iterator  begin()   const { checkValidity(); return begin_; }
  const_iterator  end()     const { checkValidity(); return end_; }
  const_reference cursor()        { checkValidity(); return cursor_; }

  size_type       size()    const { checkValidity(); return end_ - begin_; }
  size_type       sizeFwd() const { checkValidity(); return end_ - cursor_; }
  size_type       sizeBwd() const { checkValidity(); return cursor_ - begin_; }

  operator bool()           const { checkValidity(); return input_; }

  bool addData()
  {
    checkValidity();
    if ( sizeFwd() ) {
      if ( sizeFwd() < sizeBwd() ) {
        // no overlap - I can use memcpy
        memcpy  ( static_cast<void*> ( begin_ )
                , static_cast<void const*> ( cursor_ ), sizeFwd() );
      }
      else {
        memmove ( static_cast<void*> ( begin_ )
                , static_cast<void const*> ( cursor_ ), sizeFwd() );
      }      
      end_ = begin_ + sizeFwd();
      cursor_ = begin_;
    }
    uint32 bytesRead = 0, target = BUFFER_SIZE - size();
    while ( input_ && bytesRead < target ) {
      input_.read ( reinterpret_cast<char*> ( begin_ ) + size() + bytesRead
                  , target - bytesRead );
      bytesRead += input_.gcount();
    }
    end_ += bytesRead;
    filePointer_ += bytesRead;
    checkValidity();
    return bytesRead > 0;
  }
  
  uint32 filePosition() const
  {
    return filePointer_ - sizeFwd();
  }
  
private:
  void checkValidity() const
  {
    DIRAC_MESSAGE_ASSERT  ( "begin, end and cursor cannot be null"
                          , 0 != begin_ && 0 != end_ && 0 != cursor_ );
    DIRAC_MESSAGE_ASSERT  ( "end cannot exceed begin + BUFFER_SIZE"
                          , !(end_ > begin_ + BUFFER_SIZE) );
    DIRAC_MESSAGE_ASSERT  ( "cursor must be within begin and end"
                          , !( cursor_ < begin_ ) && !( cursor_ > end_ ) );
  }
  
  uint8*        begin_;
  uint8 const*  end_;
  uint8 const*  cursor_;
  std::ifstream input_;
  static uint32 filePointer_;
#undef BUFFER_SIZE
};

uint32 Bitstream::filePointer_ = 0;

#endif /* _BITSTREAM_HPP_ */
