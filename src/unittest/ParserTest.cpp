/* ***** BEGIN LICENSE BLOCK *****
*
* $Id:  $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unittest/ParserTest.hpp>
#include <unittest/core_suite.hpp>

#include <unittest/Bitstream.hpp>
#include <iostream>

#include <parser/parser.h>
#include <parser/IInput.hpp>
#include <parser/StreamReader.hpp>

#include <decoder/FileReader.hpp>


//NOTE: ensure that the suite is added to the default registry in
//cppunit_testsuite.cpp
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION (ParserTest, coreSuiteName());

#ifdef BITS
std::string ParserTest::filename_ = BITS;
#else
std::string ParserTest::filename_ = "../bitstreams/AN_schro_1Mbps.drc";
#endif

using namespace ::dirac::parser_project;
using namespace ::dirac::decoder;

ParserTest::ParserTest() {}

ParserTest::~ParserTest() {}

void 
ParserTest::setUp()
{
  data_ = new uint8[ARRAY_SIZE];
  memset ( static_cast<void*> ( data_ ), 0, ARRAY_SIZE );
  
  fillBuffer  ( data_ + SEQHDR_START, SEQUENCE_HEADER
              , SEQHDR_NEXT, SEQHDR_PREV );
  fillBuffer  ( data_ + PIC1_START, INTRA_REF_AC, PIC1_NEXT, PIC1_PREV );
  write4Bytes ( data_ + PIC1_START + 13, 0 );
  fillBuffer  ( data_ + PIC2_START, INTER_REF_AC_1REF, PIC2_NEXT, PIC2_PREV );
  write4Bytes ( data_ + PIC2_START + 13, 1 );
  fillBuffer  ( data_ + PAD_START, PADDING_DATA, PAD_NEXT, PAD_PREV );
  fillBuffer  ( data_ + PIC3_START, INTER_NON_REF_AC_1REF
              , PIC3_NEXT, PIC3_PREV );
  write4Bytes ( data_ + PIC3_START + 13, 2 );
  fillBuffer  ( data_ + AUX_START, AUX_DATA, AUX_NEXT, AUX_PREV );
  fillBuffer  ( data_ + EOS_START, END_OF_SEQUENCE, EOS_NEXT, EOS_PREV );
}

void 
ParserTest::tearDown()
{
  delete [] data_;
}
  
/*  test all getters functions (for all types of access units)  */
void 
ParserTest::testGetters()
{
  uint8 const*  buffer = data_;
  size_t        au_size = 0;

  /* dirac_parser_get_sequence_header */
  CPPUNIT_ASSERT ( dirac_parser_get_sequence_header  ( &buffer, ARRAY_SIZE
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + SEQHDR_START == buffer );
  CPPUNIT_ASSERT ( au_size == static_cast<size_t> ( SEQHDR_NEXT ) );
  buffer = data_;
  /*  shorten buffer size - it works even if there is only room for the 
      sequence header, ie no room to spare  */
  CPPUNIT_ASSERT ( dirac_parser_get_sequence_header  ( &buffer
                                                , PIC1_START
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + SEQHDR_START == buffer );
  /* forcing failure */
  CPPUNIT_ASSERT ( !dirac_parser_get_sequence_header ( &buffer
                                                , SEQHDR_NEXT - 1
                                                , &au_size ) );
  buffer = data_;
  au_size = 0;
  CPPUNIT_ASSERT ( dirac_parser_get_sequence_header  ( &buffer
                                                , ARRAY_SIZE - SEQHDR_START
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + SEQHDR_START == buffer );
  CPPUNIT_ASSERT ( au_size == static_cast<size_t> ( SEQHDR_NEXT ) );
  
  buffer = data_ + SEQHDR_START + 1;
  CPPUNIT_ASSERT ( !dirac_parser_get_sequence_header ( &buffer
                                                , ARRAY_SIZE - SEQHDR_START - 1
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + EOS_START == buffer );
  
  /*  dirac_parser_get_next_picture */
  buffer = data_;
  au_size = 0;
  CPPUNIT_ASSERT ( dirac_parser_get_next_picture ( &buffer, ARRAY_SIZE, &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC1_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC1_NEXT ) == au_size );
  ++buffer;
  CPPUNIT_ASSERT ( dirac_parser_get_next_picture ( &buffer
                                            , ARRAY_SIZE - PIC1_START - 1
                                            , &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC2_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC2_NEXT ) == au_size );
  buffer = data_ + PIC3_START;
  CPPUNIT_ASSERT ( dirac_parser_get_next_picture ( &buffer
                                            , ARRAY_SIZE - PIC3_START 
                                            , &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC3_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC3_NEXT ) == au_size );
  ++buffer;
  CPPUNIT_ASSERT ( !dirac_parser_get_next_picture  ( &buffer
                                              , ARRAY_SIZE - PIC3_START - 1
                                              , &au_size ) );
  CPPUNIT_ASSERT ( data_ + EOS_START == buffer );
  buffer = data_ + PAD_START;
  au_size = 0;
  CPPUNIT_ASSERT ( dirac_parser_get_next_picture ( &buffer
                                            , ARRAY_SIZE - PAD_START
                                            , &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC3_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC3_NEXT ) == au_size );
  
  /*  dirac_parser_get_next_access_unit */
  buffer = data_;
  au_size = 0;
  CPPUNIT_ASSERT ( dirac_parser_get_next_access_unit ( &buffer
                                                , ARRAY_SIZE
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + SEQHDR_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( SEQHDR_NEXT ) == au_size );
  CPPUNIT_ASSERT ( dirac_parser_get_next_access_unit ( &buffer
                                                , ARRAY_SIZE - SEQHDR_START 
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + SEQHDR_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( SEQHDR_NEXT ) == au_size );
  buffer = data_ + PIC1_START;
  CPPUNIT_ASSERT ( dirac_parser_get_next_access_unit ( &buffer
                                                , ARRAY_SIZE - PIC1_START
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC1_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC1_NEXT ) == au_size );
  ++buffer;
  au_size = 0;
  CPPUNIT_ASSERT ( dirac_parser_get_next_access_unit ( &buffer
                                                , ARRAY_SIZE - PIC1_START - 1
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC2_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC2_NEXT ) == au_size );
  buffer = data_ + EOS_START - 1;
  au_size = 0;
  CPPUNIT_ASSERT ( dirac_parser_get_next_access_unit ( &buffer
                                                , ARRAY_SIZE - EOS_START + 1
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + EOS_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( 13 ) == au_size );
  ++buffer;
  au_size = 0;
  CPPUNIT_ASSERT ( !dirac_parser_get_next_access_unit  ( &buffer
                                                  , ARRAY_SIZE - EOS_START - 1
                                                  , &au_size ) );
  CPPUNIT_ASSERT ( data_ + ARRAY_SIZE - 3 == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( 0 ) == au_size );
  
  /*  dirac_parser_get_next_i_picture */
  buffer = data_;
  au_size = 0;
  CPPUNIT_ASSERT ( dirac_parser_get_next_i_picture ( &buffer, ARRAY_SIZE, &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC1_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC1_NEXT ) == au_size );
  ++buffer;
  au_size = 0;
  CPPUNIT_ASSERT ( !dirac_parser_get_next_i_picture  ( &buffer
                                                , ARRAY_SIZE - PIC1_START - 1
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + EOS_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( 0 ) == au_size );
  
  /*  dirac_parser_get_i_picture_skip_n */
  buffer = data_;
  au_size = 0;
  int n = 0;
  CPPUNIT_ASSERT ( dirac_parser_get_i_picture_skip_n ( &buffer
                                                , ARRAY_SIZE // len_fwd
                                                , 0          // len_bwd
                                                , &n          // n
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC1_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC1_NEXT ) == au_size );
  CPPUNIT_ASSERT ( 0 == n );
  buffer = data_;
  au_size = 0;
  n = 1;
  CPPUNIT_ASSERT ( !dirac_parser_get_i_picture_skip_n  ( &buffer
                                                  , ARRAY_SIZE // len_fwd
                                                  , 0          // len_bwd
                                                  , &n          // n
                                                  , &au_size ) );
  CPPUNIT_ASSERT ( data_ + EOS_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( 0 ) == au_size );
  CPPUNIT_ASSERT ( 0 == n );
  buffer = data_ + EOS_START + 100;
  au_size = 0;
  n = -1;
  CPPUNIT_ASSERT ( dirac_parser_get_i_picture_skip_n ( &buffer
                                                , ARRAY_SIZE - EOS_START - 100
                                                , EOS_START + 100
                                                , &n
                                                , &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC1_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC1_NEXT ) == au_size );
  CPPUNIT_ASSERT ( 0 == n );
  buffer = data_ + EOS_START + 100;
  au_size = 0;
  n = -2;
  CPPUNIT_ASSERT ( !dirac_parser_get_i_picture_skip_n ( &buffer
                                                 , ARRAY_SIZE - EOS_START - 100
                                                 , EOS_START + 100
                                                 , &n 
                                                 , &au_size ) );
  CPPUNIT_ASSERT ( data_ + 3 == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( 0 ) == au_size );
  CPPUNIT_ASSERT ( -1 == n );
  
  /* dirac_parser_get_picture_skip_n */
  buffer = data_;
  au_size = 0;
  n = 1; // look for second picture in stream from start
  CPPUNIT_ASSERT ( dirac_parser_get_picture_skip_n ( &buffer
                                                   , ARRAY_SIZE
                                                   , 0
                                                   , &n
                                                   , &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC2_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC2_NEXT ) == au_size );
  CPPUNIT_ASSERT ( 0 == n );
  
  au_size = 0;
  n = 1; // pick 3 pic now
  CPPUNIT_ASSERT ( dirac_parser_get_picture_skip_n ( &buffer
                                                   , ARRAY_SIZE - PIC2_START
                                                   , PIC2_START
                                                   , &n
                                                   , &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC3_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC3_NEXT ) == au_size );
  CPPUNIT_ASSERT ( 0 == n );
  
  n = -2;
  au_size = 0;
  CPPUNIT_ASSERT ( dirac_parser_get_picture_skip_n ( &buffer
                                                   , ARRAY_SIZE - PIC3_START 
                                                   , PIC3_START
                                                   , &n
                                                   , &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC1_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC1_NEXT ) == au_size );
  CPPUNIT_ASSERT ( 0 == n );
  
  n = 10;
  au_size = 0;
  CPPUNIT_ASSERT ( !dirac_parser_get_picture_skip_n ( &buffer
                                                    , ARRAY_SIZE - PIC1_START
                                                    , PIC1_START
                                                    , &n
                                                    , &au_size ) );
  CPPUNIT_ASSERT ( data_ + EOS_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( 0 ) == au_size );
  CPPUNIT_ASSERT ( 7 == n );
  
  /*  dirac_parser_get_previous_i_picture */
  buffer = data_ + ARRAY_SIZE;
  au_size = 0;
  CPPUNIT_ASSERT ( dirac_parser_get_previous_i_picture ( &buffer
                                                  , 0
                                                  , ARRAY_SIZE
                                                  , &au_size ) );
  CPPUNIT_ASSERT ( data_ + PIC1_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( PIC1_NEXT ) == au_size );
  CPPUNIT_ASSERT ( !dirac_parser_get_previous_i_picture  ( &buffer
                                                    , ARRAY_SIZE - PIC1_START
                                                    , PIC1_START
                                                    , &au_size ) );
  CPPUNIT_ASSERT ( data_ + 3 == buffer );
}


void 
ParserTest::testMoveCursorForward()
{
  uint8 const* buffer   = data_;
  size_t       au_size  = 0;
  CPPUNIT_ASSERT ( dirac_parser_get_next_access_unit ( &buffer, ARRAY_SIZE, &au_size)
          &&  dirac_parser_move_cursor_fwd ( &buffer, ARRAY_SIZE - SEQHDR_START ) );
  CPPUNIT_ASSERT ( data_ + PIC1_START == buffer );
  CPPUNIT_ASSERT ( static_cast<size_t> ( SEQHDR_NEXT ) == au_size );
  CPPUNIT_ASSERT ( dirac_parser_move_cursor_fwd ( &buffer, ARRAY_SIZE - PIC1_START ) );
  CPPUNIT_ASSERT ( data_ + PIC2_START == buffer );
  CPPUNIT_ASSERT ( dirac_parser_move_cursor_fwd ( &buffer, ARRAY_SIZE - PIC2_START ) );
  CPPUNIT_ASSERT ( data_ + PAD_START == buffer );
  buffer = data_ + EOS_START;
  CPPUNIT_ASSERT ( !dirac_parser_move_cursor_fwd ( & buffer, ARRAY_SIZE - EOS_START ) );
  CPPUNIT_ASSERT ( data_ + EOS_START == buffer );
}

/*  test all access units identity checks */
void 
ParserTest::testAccessUnitIdentityChecks()
{
  uint8 const*  buffer = data_;
  CPPUNIT_ASSERT ( !dirac_parser_is_access_unit ( buffer, ARRAY_SIZE ) );
  buffer = data_ + SEQHDR_START;
  CPPUNIT_ASSERT ( dirac_parser_is_access_unit ( buffer, ARRAY_SIZE - SEQHDR_START ) );
  buffer = data_ + PIC1_START;
  CPPUNIT_ASSERT ( dirac_parser_is_access_unit ( buffer, ARRAY_SIZE - PIC1_START ) );
  buffer = data_ + PAD_START;
  CPPUNIT_ASSERT ( dirac_parser_is_access_unit ( buffer, ARRAY_SIZE - PAD_START ) );
  buffer = data_ + PIC2_START;
  CPPUNIT_ASSERT ( dirac_parser_is_access_unit ( buffer, ARRAY_SIZE - PIC2_START ) );
  buffer = data_ + AUX_START;
  CPPUNIT_ASSERT ( dirac_parser_is_access_unit ( buffer, ARRAY_SIZE - AUX_START ) );
  buffer = data_ + EOS_START;
  CPPUNIT_ASSERT ( dirac_parser_is_access_unit ( buffer, ARRAY_SIZE - EOS_START ) );
  buffer = data_ + AUX_START + 1;
  CPPUNIT_ASSERT ( !dirac_parser_is_access_unit  ( buffer
                                            , ARRAY_SIZE - AUX_START - 1 ) );
  buffer = data_ + EOS_START + 100;
  CPPUNIT_ASSERT ( !dirac_parser_is_access_unit ( buffer, ARRAY_SIZE - EOS_START - 100 ) );
  
  buffer = data_ + SEQHDR_START;
  CPPUNIT_ASSERT ( dirac_parser_is_sequence_header ( buffer, ARRAY_SIZE - SEQHDR_START ) );
  ++buffer;
  CPPUNIT_ASSERT ( !dirac_parser_is_sequence_header  ( buffer
                                                , ARRAY_SIZE - SEQHDR_START - 1 ) );
  buffer = data_ + PIC2_START;
  CPPUNIT_ASSERT ( !dirac_parser_is_sequence_header ( buffer, ARRAY_SIZE - PIC2_START ) );
  buffer = data_ + EOS_START;
  CPPUNIT_ASSERT ( !dirac_parser_is_sequence_header ( buffer, ARRAY_SIZE - EOS_START ) );
  
  buffer = data_ + SEQHDR_START;
  CPPUNIT_ASSERT ( !dirac_parser_is_picture ( buffer, ARRAY_SIZE - SEQHDR_START ) );
  buffer = data_ + PIC1_START;
  CPPUNIT_ASSERT ( dirac_parser_is_picture ( buffer, ARRAY_SIZE - PIC1_START ) );
  buffer = data_ + PIC2_START;
  CPPUNIT_ASSERT ( dirac_parser_is_picture ( buffer, ARRAY_SIZE - PIC2_START ) );
  buffer = data_ + PAD_START;
  CPPUNIT_ASSERT ( !dirac_parser_is_picture ( buffer, ARRAY_SIZE - PAD_START ) );
  buffer = data_ + PIC3_START;
  CPPUNIT_ASSERT ( dirac_parser_is_picture ( buffer, ARRAY_SIZE - PIC3_START ) );
  buffer = data_ + EOS_START;
  CPPUNIT_ASSERT ( !dirac_parser_is_picture ( buffer, ARRAY_SIZE - EOS_START ) );
  
  buffer = data_ + SEQHDR_START;
  CPPUNIT_ASSERT ( !dirac_parser_is_i_picture ( buffer, ARRAY_SIZE - SEQHDR_START ) );
  buffer = data_ + PIC1_START;
  CPPUNIT_ASSERT ( dirac_parser_is_i_picture ( buffer, ARRAY_SIZE - PIC1_START ) );
  buffer = data_ + PIC2_START;
  CPPUNIT_ASSERT ( !dirac_parser_is_i_picture ( buffer, ARRAY_SIZE - PIC2_START ) );
  buffer = data_ + PAD_START;
  CPPUNIT_ASSERT ( !dirac_parser_is_i_picture ( buffer, ARRAY_SIZE - PAD_START ) );
  buffer = data_ + PIC3_START;
  CPPUNIT_ASSERT ( !dirac_parser_is_i_picture ( buffer, ARRAY_SIZE - PIC3_START ) );
  buffer = data_ + EOS_START;
  CPPUNIT_ASSERT ( !dirac_parser_is_i_picture ( buffer, ARRAY_SIZE - EOS_START ) );
  
  buffer = data_ + PAD_START;
  CPPUNIT_ASSERT ( !dirac_parser_is_end_of_sequence ( buffer, ARRAY_SIZE - PAD_START ) );
  buffer = data_ + EOS_START;
  CPPUNIT_ASSERT ( dirac_parser_is_end_of_sequence ( buffer, ARRAY_SIZE - EOS_START ) );
  ++buffer;
  CPPUNIT_ASSERT ( !dirac_parser_is_end_of_sequence ( buffer, ARRAY_SIZE - EOS_START - 1 ) );
  
  buffer = data_ + PAD_START;
  CPPUNIT_ASSERT ( dirac_parser_is_padding_data ( buffer, ARRAY_SIZE - PAD_START ) );
  ++buffer;
  CPPUNIT_ASSERT ( !dirac_parser_is_padding_data ( buffer, ARRAY_SIZE - PAD_START - 1 ) );
  
  buffer = data_ + AUX_START;
  CPPUNIT_ASSERT ( dirac_parser_is_auxiliary_data ( buffer, ARRAY_SIZE - AUX_START ) );
  ++buffer;
  CPPUNIT_ASSERT ( !dirac_parser_is_auxiliary_data ( buffer, ARRAY_SIZE - AUX_START - 1 ) );
}

void 
ParserTest::testGetPictureNumber()
{
  uint8 const*  buffer  = data_;
  size_t        au_size = 0;
  uint32        picNumber = 0xFFFFFFFF;
  dirac_parser_get_next_picture ( &buffer, ARRAY_SIZE, &au_size );
  CPPUNIT_ASSERT ( data_ + PIC1_START == buffer );
  CPPUNIT_ASSERT ( dirac_parser_get_picture_number ( buffer
                                              , data_ + ARRAY_SIZE - buffer
                                              , &picNumber ) );
  CPPUNIT_ASSERT ( static_cast<uint32> ( 0 ) == picNumber );
  CPPUNIT_ASSERT ( data_ + PIC1_START == buffer ); // we haven't moved buffer
  CPPUNIT_ASSERT ( dirac_parser_move_cursor_fwd  ( &buffer
                                            , data_ + ARRAY_SIZE - buffer ) );
  dirac_parser_get_next_picture ( &buffer
                                , data_ + ARRAY_SIZE - buffer, &au_size );
  CPPUNIT_ASSERT ( data_ + PIC2_START == buffer );
  CPPUNIT_ASSERT ( dirac_parser_get_picture_number ( buffer
                                              , data_ + ARRAY_SIZE - buffer
                                              , &picNumber ) );
  CPPUNIT_ASSERT ( static_cast<uint32> ( 1 ) == picNumber );
  CPPUNIT_ASSERT ( data_ + PIC2_START == buffer );
  CPPUNIT_ASSERT ( dirac_parser_move_cursor_fwd  ( &buffer
                                            , data_ + ARRAY_SIZE - buffer ) );
  dirac_parser_get_next_picture ( &buffer
                                , data_ + ARRAY_SIZE - buffer, &au_size );
  CPPUNIT_ASSERT ( data_ + PIC3_START == buffer );
  CPPUNIT_ASSERT ( dirac_parser_get_picture_number ( buffer
                                              , data_ + ARRAY_SIZE - buffer
                                              , &picNumber ) );
  CPPUNIT_ASSERT ( static_cast<uint32> ( 2 ) == picNumber );
  CPPUNIT_ASSERT ( data_ + PIC3_START == buffer );
}

/*  test the Bitstream class, used in other tests   */
void 
ParserTest::testBitstream()
{
  Bitstream bitstream ( filename_ );
  CPPUNIT_ASSERT ( bitstream );
  CPPUNIT_ASSERT ( bitstream.sizeBwd() + bitstream.sizeFwd()
                 == static_cast<uint32> ( 10000 ) );
  CPPUNIT_ASSERT ( static_cast<uint32> ( 0 ) == bitstream.filePosition() );
  CPPUNIT_ASSERT ( bitstream.begin() == bitstream.cursor() );
  CPPUNIT_ASSERT ( bitstream.begin() + 10000 == bitstream.end() );
  
  bitstream.cursor() = bitstream.end() - 100;
  CPPUNIT_ASSERT ( bitstream.end() - 100 == bitstream.cursor() );
  CPPUNIT_ASSERT ( static_cast<uint32> ( 10000 - 100 )
                 == bitstream.filePosition() );
  int i = 1;
  while ( bitstream.addData() ) {
    CPPUNIT_ASSERT ( bitstream.begin() == bitstream.cursor() );
    CPPUNIT_ASSERT ( i++ * static_cast<uint32> ( 10000 - 100 )
                    == bitstream.filePosition() );    
    bitstream.cursor() = bitstream.end() - 100;
  }
}

/*  test a real bitstream */
void 
ParserTest::testDiracStream()
{
  std::auto_ptr<IInput> input ( new FileReader ( filename_ ) );
  CPPUNIT_ASSERT ( input->isAvailable() );
  const size_t STREAM_SIZE = 5; // buffersize = STREAM_SIZE * 4096 * 10
  std::auto_ptr<StreamReader> str ( new StreamReader ( input, STREAM_SIZE ) );

  uint32        seqHdrCounter       = 0;
  uint32        picCounter          = 0;
  uint32        iPicCounter         = 0;
  uint32        auxCounter          = 0;
  uint32        padCounter          = 0;
  uint32        eosCounter          = 0;
  uint32        totalSize           = 0;
  uint8 const*& cursor              = str->gptr();
  size_t        auSize              = 0;
  /*  move cursor to start of first PIH */
  CPPUNIT_ASSERT ( dirac_parser_get_next_access_unit  ( &cursor
                                                , str->sizeFwd()
                                                , &auSize ) );
  do {
    do {
      if ( dirac_parser_is_picture ( cursor, str->sizeFwd() ) ) {
        ++picCounter;
        CPPUNIT_ASSERT ( dirac_parser_get_next_picture ( &cursor
                                                  , str->sizeFwd()
                                                  , &auSize ) );
        totalSize += auSize;
        if ( dirac_parser_is_i_picture ( cursor, str->sizeFwd() ) ) {
          ++iPicCounter;
        }
      }
      else if ( dirac_parser_is_sequence_header ( cursor, str->sizeFwd() ) ) {
        ++seqHdrCounter;
        CPPUNIT_ASSERT ( dirac_parser_get_sequence_header ( &cursor
                                                     , str->sizeFwd()
                                                     , &auSize ) );
        totalSize += auSize;
      }
      else if ( dirac_parser_is_auxiliary_data ( cursor, str->sizeFwd()) ) {
        ++auxCounter;
        CPPUNIT_ASSERT ( dirac_parser_get_next_access_unit ( &cursor
                                                      , str->sizeFwd()
                                                      , &auSize ) );
        totalSize += auSize;
      }
      else if ( dirac_parser_is_padding_data ( cursor, str->sizeFwd() ) ) {
        ++padCounter;
        CPPUNIT_ASSERT ( dirac_parser_get_next_access_unit ( &cursor
                                                      , str->sizeFwd()
                                                      , &auSize ) );
        totalSize += auSize;
      }
      else if ( dirac_parser_is_end_of_sequence ( cursor, str->sizeFwd() ) ) {
        ++eosCounter;
        CPPUNIT_ASSERT ( dirac_parser_get_next_access_unit ( &cursor
                                                      , str->sizeFwd()
                                                      , &auSize ) );
        totalSize += auSize;
      }
      else if ( dirac_parser_is_access_unit ( cursor, str->sizeFwd() ) ) {
        std::cout << "File position: " << str->goffset() << std::endl;
        CPPUNIT_ASSERT ( dirac_parser_get_next_access_unit ( &cursor
                                                      , str->sizeFwd()
                                                      , &auSize ) );
        totalSize += auSize;          
      }
    }   while ( dirac_parser_move_cursor_fwd   ( &cursor
                                                , str->sizeFwd() ) );
    // can't move the cursor any further, I need more data
  } while ( str->addDataFwd() );

  
  using std::cout;
  using std::endl;
  cout << endl;
  cout << "Number of pics:                " << picCounter << endl;
  cout << "Number of i pics:              " << iPicCounter << endl;
  cout << "Number of aux data:            " << auxCounter << endl;
  cout << "Number of pad data:            " << padCounter << endl;
  cout << "Number of eos:                 " << eosCounter << endl;
  cout << "Number of seqhdr:              " << seqHdrCounter << endl;
  cout << "Total size decoded:            " << totalSize << endl;
  cursor = str->egptr();
  cout << "Total file size:               " << str->goffset() << endl;
}

void 
ParserTest::fillBuffer  ( uint8* buffer, uint8 parseCode
                        , uint32 next, uint32 prev)
{
  *buffer++ = 0x42;
  *buffer++ = 0x42;
  *buffer++ = 0x43;
  *buffer++ = 0x44;
  *buffer++ = parseCode;
  write4Bytes ( buffer, next );
  write4Bytes ( buffer + 4, prev );
}

void 
ParserTest::write4Bytes ( uint8* buffer, uint32 data )
{
  *buffer++ = static_cast<uint8> ( ( data & 0xFF000000 ) >> 24 );
  *buffer++ = static_cast<uint8> ( ( data & 0x00FF0000 ) >> 16 );
  *buffer++ = static_cast<uint8> ( ( data & 0x0000FF00 ) >> 8 );
  *buffer   = static_cast<uint8> ( data & 0x000000FF );
}


