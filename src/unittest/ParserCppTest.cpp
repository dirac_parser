/* ***** BEGIN LICENSE BLOCK *****
*
* $Id:  $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unittest/ParserCppTest.hpp>
#include <unittest/core_suite.hpp>

#include <parser/Parser.hpp>
#include <parser/StreamReader.hpp>

#include <parser/AccessUnit.hpp>
#include <decoder/FileReader.hpp>

#include <iostream>
#include <memory>

using namespace ::dirac::parser_project;
using namespace ::dirac::decoder;


//NOTE: ensure that the suite is added to the default registry in
//cppunit_testsuite.cpp
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION (ParserCppTest, coreSuiteName());

#ifdef BITS
std::string ParserCppTest::filename_ = BITS;
#else
std::string ParserCppTest::filename_ = 
        "/project/compression/andrea/workspace/bitstreams/BR_schro_2Mbps.drc";
#endif

ParserCppTest::ParserCppTest() {}

ParserCppTest::~ParserCppTest() {}

void 
ParserCppTest::setUp() {}

void 
ParserCppTest::tearDown() {}

void 
ParserCppTest::testParser()
{
  std::auto_ptr<IInput> input ( new FileReader ( filename_ ) );
  CPPUNIT_ASSERT ( input->isAvailable() );
  const size_t STREAM_SIZE = 5; // buffersize = STREAM_SIZE * 4096 * 10
  StreamReader* debug_str = new StreamReader ( input, STREAM_SIZE );
  std::auto_ptr<StreamReader> str ( debug_str );
  Parser parser ( str );
  AccessUnit au;
  CPPUNIT_ASSERT ( parser.getNextSequenceHeader ( au ) );
  CPPUNIT_ASSERT ( au.isSequenceHeader() );
  
  CPPUNIT_ASSERT ( parser.getNextPicture( au ) );
  CPPUNIT_ASSERT ( au.isPicture() );
  CPPUNIT_ASSERT ( au.isIPicture() );
  
  CPPUNIT_ASSERT ( parser.moveCursorFwd() );
  CPPUNIT_ASSERT ( parser.getNextIPicture ( au ) );
  CPPUNIT_ASSERT ( au.isPicture() );
  CPPUNIT_ASSERT ( au.isIPicture() );

  CPPUNIT_ASSERT ( parser.getNextIPictureSkipN ( au, -1 ) );
  
}

void 
ParserCppTest::testFullRun()
{
  std::auto_ptr<IInput> input ( new FileReader ( filename_ ) );
  CPPUNIT_ASSERT ( input->isAvailable() );
  const size_t STREAM_SIZE = 10; // buffersize = STREAM_SIZE * 4096 * 10
  StreamReader* debug_str = new StreamReader ( input, STREAM_SIZE );
  std::auto_ptr<StreamReader> str ( debug_str );
  Parser parser ( str );
  AccessUnit au;
  int count = 0;
  int ipic = 0;
  int pic = 0;
  do {
    CPPUNIT_ASSERT ( parser.getNextAccessUnit ( au ) );
    ++count;      
    if ( au.isPicture() ) {
      ++pic;
      if ( au.isIPicture() ) ++ipic;
    }
  } while ( parser.moveCursorFwd() );
  std::cout << std::endl << std::endl;
  std::cout << "count: " << count;
  std::cout << "  pic count: " << pic << "  i pic count: " << ipic << std::endl;
  if ( parser.rewind() ) {
    count = ipic = pic = 0;
    do {
      CPPUNIT_ASSERT ( parser.getNextAccessUnit ( au ) );
      ++count;
      if ( au.isPicture() ) {
        ++pic;
        if ( au.isIPicture() ) ++ipic;
      }
    } while ( parser.moveCursorFwd() );
    std::cout << "After rewind: count: " << count;
    std::cout << "  pic count: " << pic << "  i pic count: " << ipic << std::endl;
  }
}

