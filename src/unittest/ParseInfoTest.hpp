/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: ParseInfoTestSuite.hpp 48 2008-02-12 22:48:00Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _PARSEINFOTEST_HPP_
#define _PARSEINFOTEST_HPP_

#include <cppunit/extensions/HelperMacros.h>
#include <common/dirac.h>

class ParseInfoTest : public CPPUNIT_NS::TestFixture
{
  CPPUNIT_TEST_SUITE ( ParseInfoTest );
  CPPUNIT_TEST ( testIsParseInfo );
  CPPUNIT_TEST ( testIsSequenceHeader );
  CPPUNIT_TEST ( testIsEndOfSequence );
  CPPUNIT_TEST ( testIsAuxiliaryData );
  CPPUNIT_TEST ( testIsPaddingData );
  CPPUNIT_TEST ( testIsPicture );
  CPPUNIT_TEST ( testIsCoreSyntax );
  CPPUNIT_TEST ( testIsLowDelaySyntax );
  CPPUNIT_TEST ( testIsUsingArithmeticCoding );
  CPPUNIT_TEST ( testIsReference );
  CPPUNIT_TEST ( testIsNotReference );
  CPPUNIT_TEST ( testIsIntra );
  CPPUNIT_TEST ( testIsInter );
  CPPUNIT_TEST ( testGetNumberOfReferences );
  CPPUNIT_TEST ( testHasPayload );
  CPPUNIT_TEST ( testNext );
  CPPUNIT_TEST ( testPrevious );
  CPPUNIT_TEST ( testSize );
  CPPUNIT_TEST_SUITE_END();
  
  public:
    ParseInfoTest();
    virtual ~ParseInfoTest();
    
    virtual void setUp();
    virtual void tearDown();
    
    void testIsParseInfo();
    void testIsSequenceHeader();
    void testIsEndOfSequence();
    void testIsAuxiliaryData();
    void testIsPaddingData();
    void testIsPicture();
    void testIsCoreSyntax();
    void testIsLowDelaySyntax();
    void testIsUsingArithmeticCoding();
    void testIsReference();
    void testIsNotReference();
    void testIsIntra();
    void testIsInter();
    void testGetNumberOfReferences();
    void testHasPayload();
    void testNext();
    void testPrevious();
    void testSize();
    
  private:  // members + helpers
    void fillBuffer ( uint8* buffer, uint8 parseCode, uint32 next, uint32 prev);
    void write4Bytes ( uint8* buffer, uint32 data );
        
    enum {
        DATA_SIZE   = 1000
      , FIRST_PIH   = 0
      , SECOND_PIH  = 100
      , THIRD_PIH   = 900
      , PIH_1       = 0x42
      , PIH_2       = 0x42
      , PIH_3       = 0x43
      , PIH_4       = 0x44
      , PI_CODE_OFF = 4
      , NPO_OFF     = 5
      , PPO_OFF     = 9
    };

    /*  Based on DiracSpec1.0.0_pre11, 20 Dec 2007  */
    enum Parsecodes {
        SEQUENCE_HEADER         = 0x00
      , END_OF_SEQUENCE         = 0x10
      , AUX_DATA                = 0x20
      , PADDING_DATA            = 0x30
      , INTRA_REF_AC            = 0x0C
      , INTRA_NONREF_AC         = 0x08
      , INTRA_REF_NOAC          = 0x4C
      , INTRA_NONREF_NOAC       = 0x48
      , INTER_REF_AC_1REF       = 0x0D
      , INTER_REF_AC_2REF       = 0x0E
      , INTER_NON_REF_AC_1REF   = 0x09
      , INTER_NON_REF_AC_2REF   = 0x0A
      /* low delay syntax */
      , INTRA_REF_LD            = 0xCC
      , INTRA_NON_REF_LD        = 0xC8
    };
    
    uint8* data_;
    uint8* buffer;
};


#endif /* _PARSEINFOTEST_HPP_ */
