/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: ScannerTestSuite.hpp 37 2008-02-07 09:18:30Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _SCANNERTEST_HPP_
#define _SCANNERTEST_HPP_

#include <cppunit/extensions/HelperMacros.h>
#include <common/dirac.h>


// we are going to use the real Buffer object here, rather than a stub or mock
class ScannerTest : public CPPUNIT_NS::TestFixture
{
  CPPUNIT_TEST_SUITE ( ScannerTest );
  CPPUNIT_TEST ( testScanForward );
  CPPUNIT_TEST ( testScanBackward );
  CPPUNIT_TEST ( testCursor );
  CPPUNIT_TEST ( testSizeFwd );
  CPPUNIT_TEST ( testSizeBwd );
  CPPUNIT_TEST_SUITE_END();
  
  public:
    ScannerTest();
    virtual ~ScannerTest();
    
    virtual void setUp();
    virtual void tearDown();
    
    void testScanForward();
    void testScanBackward();
    void testCursor();
    void testSizeFwd();
    void testSizeBwd();
    
  private: // helper methods + members
    void fillBuffer ( uint8* buffer, uint8 parseCode, uint32 next, uint32 prev);
    void write4Bytes ( uint8* buffer, uint32 data );
    
    enum {
        SCAN_DATA_SIZE    = 10000
      , FIRST_PIH         = 100
      , FIRST_FWD_OFFS    = 200
      , FIRST_BWD_OFFS    = 0
      , SECOND_PIH        = 300
      , SECOND_FWD_OFFS   = 400
      , SECOND_BWD_OFFS   = 200
      , THIRD_PIH         = 700
      , THIRD_FWD_OFFS    = 500
      , THIRD_BWD_OFFS    = 400
    };
    
    uint8* data_;
};

#endif /* _SCANNERTEST_HPP_ */
