/* ***** BEGIN LICENSE BLOCK *****
*
* $Id:  $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unittest/SchrodecTest.hpp>
#include <unittest/core_suite.hpp>

#include <decoder/Schrodec.hpp>

#include <parser/StreamReader.hpp>
#include <decoder/FileReader.hpp>
#include <decoder/FileWriter.hpp>
#include <decoder/DefaultFrameMemoryManager.hpp>
#include <decoder/FrameManagerMaker.hpp>

#include <iostream>
#include <cstdio>

using namespace ::dirac::decoder;
using namespace ::dirac::parser_project;

//NOTE: ensure that the suite is added to the default registry in
//cppunit_testsuite.cpp
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION (SchrodecTest, coreSuiteName());

#ifdef BITS
std::string SchrodecTest::filename_ = BITS;
#else
std::string SchrodecTest::filename_ = "../bitstreams/AN_schro_1Mbps.drc";
#endif

SchrodecTest::SchrodecTest() {}

SchrodecTest::~SchrodecTest() {}

void 
SchrodecTest::setUp()
{
  std::auto_ptr<IInput> input ( new FileReader ( filename_ ) );
  std::auto_ptr<StreamReader> reader ( new StreamReader ( input, 50 ) );
  std::auto_ptr<Parser> tmp ( new Parser ( reader ) );
  parser_ = tmp;

  std::auto_ptr<IWriter<> > writer ( new FileWriter ( filename_ + ".i420" ) );
  std::auto_ptr<IFrameMemoryManager<> > frameMemMgr 
                                            ( new DefaultFrameMemoryManager );
  std::auto_ptr<IFrameManager> manager 
                                 ( makeFrameManager ( writer, frameMemMgr ) );

  manager_ = manager;
}

void 
SchrodecTest::tearDown()
{
  std::string tmp = filename_ + ".i420";
  remove ( tmp.c_str() );
}

void 
SchrodecTest::testSchrodec()
{
  Schrodec schro ( parser_, manager_ );
  std::auto_ptr<IDecoderState> decoderState;
  do {
    std::auto_ptr<IDecoderState> tmp ( schro.getState() );
    decoderState = tmp; // the previous DecoderState gets deleted here
    decoderState->doNextAction();
  } while ( !decoderState->isEndOfSequence() ); 
  uint32 nframes1 = schro.numberOfDecodedFrames();
  std::cout << "Schro decoder decoded " << nframes1;
  std::cout << " frames." << std::endl;    

  std::auto_ptr<Parser> parser = schro.releaseParser();
  parser->rewind();
  schro.reacquireParser ( parser );
  do {
    std::auto_ptr<IDecoderState> tmp ( schro.getState() );
    decoderState = tmp; // the previous DecoderState gets deleted here
    decoderState->doNextAction();
  } while ( !decoderState->isEndOfSequence() );     

  uint32 nframes2 = schro.numberOfDecodedFrames();
  std::cout << "After rewinding, Schro decoder decoded " << nframes2;
  std::cout << " frames." << std::endl; 
  
  CPPUNIT_ASSERT_EQUAL ( nframes1, nframes2 );
}

void 
SchrodecTest::testSchrodecRun()
{
  Schrodec schro ( parser_, manager_ );
  schro.decode();
  std::cout << "Schroedinger decoder decoded " << schro.numberOfDecodedFrames();
  std::cout << " frames." << std::endl;
}




