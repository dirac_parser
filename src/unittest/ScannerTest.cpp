/* ***** BEGIN LICENSE BLOCK *****
*
* $Id:  $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unittest/ScannerTest.hpp>
#include <unittest/core_suite.hpp>

#include <parser/scanner.hpp>

#include <cstring>

using ::dirac::parser_project::Scanner;

//NOTE: ensure that the suite is added to the default registry in
//cppunit_testsuite.cpp
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION (ScannerTest, coreSuiteName());


ScannerTest::ScannerTest() {}

ScannerTest::~ScannerTest() {}

void 
ScannerTest::setUp()
{
  data_ = new uint8[SCAN_DATA_SIZE];
  memset ( static_cast<void*> ( data_ ), 0, SCAN_DATA_SIZE );
  fillBuffer ( data_ + FIRST_PIH, 0x00, FIRST_FWD_OFFS, FIRST_BWD_OFFS );
  fillBuffer ( data_ + SECOND_PIH, 0x00, SECOND_FWD_OFFS, SECOND_BWD_OFFS );
  fillBuffer ( data_ + THIRD_PIH, 0x10, 0, THIRD_BWD_OFFS );
}

void 
ScannerTest::tearDown()
{
  delete [] data_;
}

void 
ScannerTest::testScanForward()
{
  uint8 const* pih = data_;
  Scanner scanner ( pih, static_cast<uint32> ( SCAN_DATA_SIZE ) );
  CPPUNIT_ASSERT ( scanner.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + FIRST_PIH );
  /*  we haven't moved the cursor forward, if we call scan again it won't 
      do anything  */
  CPPUNIT_ASSERT ( scanner.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + FIRST_PIH );
  /* now let's move the cursor forward  */
  pih = data_ + SECOND_PIH;
  CPPUNIT_ASSERT ( scanner.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + SECOND_PIH );
  
  ++pih;
  CPPUNIT_ASSERT ( scanner.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + THIRD_PIH );
  
  pih = data_ + THIRD_PIH + 1;
  CPPUNIT_ASSERT ( !scanner.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + SCAN_DATA_SIZE - 3 );
  
  /*  let's now add a PIH prefix right at the end of the buffer, with
      no space for the rest of the PIH. I would expect the scan to fail
      and the cursor to be left at the beginning of the PIH prefix   */ 
  pih = data_ + THIRD_PIH + 1;
  write4Bytes ( data_ + SCAN_DATA_SIZE - 5, 0x42424344 );
  CPPUNIT_ASSERT ( !scanner.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + SCAN_DATA_SIZE - 5 );
  
  pih = data_;
  Scanner scanner2 ( pih, static_cast<uint32> ( THIRD_PIH + 13 ) );
  CPPUNIT_ASSERT ( scanner2.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + FIRST_PIH );
  pih = data_ + THIRD_PIH;
  CPPUNIT_ASSERT ( scanner2.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + THIRD_PIH );
}

void 
ScannerTest::testScanBackward()
{
  uint8 const* pih = data_ + THIRD_PIH + 100;
  Scanner scanner ( pih, data_ + SCAN_DATA_SIZE - pih, pih - data_ );
  CPPUNIT_ASSERT ( scanner.scanBackward() );
  CPPUNIT_ASSERT ( pih == data_ + SECOND_PIH );
  
  CPPUNIT_ASSERT ( scanner.scanBackward() );
  CPPUNIT_ASSERT ( pih == data_ + FIRST_PIH );
  
  CPPUNIT_ASSERT ( !scanner.scanBackward() );
  CPPUNIT_ASSERT ( pih == data_ + 3 );
  
  pih = data_ + THIRD_PIH;
  CPPUNIT_ASSERT ( scanner.scanBackward() );
  CPPUNIT_ASSERT ( pih == data_ + SECOND_PIH );
}

void 
ScannerTest::testCursor()
{
 uint8 const* pih = data_;
 Scanner scanner ( pih, static_cast<uint32> ( SCAN_DATA_SIZE ) );
 CPPUNIT_ASSERT ( scanner.scanForward() );
 CPPUNIT_ASSERT ( pih == data_ + FIRST_PIH );
 CPPUNIT_ASSERT ( pih == scanner.cursor() );
 /*  we haven't moved the cursor forward, if we call scan again it won't 
     do anything  */
 CPPUNIT_ASSERT ( scanner.scanForward() );
 CPPUNIT_ASSERT ( pih == data_ + FIRST_PIH );
 CPPUNIT_ASSERT ( pih == scanner.cursor() );
 /* now let's move the cursor forward  */
 pih = data_ + SECOND_PIH;
 CPPUNIT_ASSERT ( scanner.scanForward() );
 CPPUNIT_ASSERT ( pih == data_ + SECOND_PIH );
 CPPUNIT_ASSERT ( pih == scanner.cursor() );

 ++pih;
 CPPUNIT_ASSERT ( scanner.scanForward() );
 CPPUNIT_ASSERT ( pih == data_ + THIRD_PIH );
 CPPUNIT_ASSERT ( pih == scanner.cursor() );
}

void 
ScannerTest::testSizeFwd()
{
  uint8 const* pih = data_;
  Scanner scanner ( pih, static_cast<uint32> ( SCAN_DATA_SIZE ) );
  CPPUNIT_ASSERT ( scanner.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + FIRST_PIH );
  CPPUNIT_ASSERT ( data_ + SCAN_DATA_SIZE - pih == scanner.sizeFwd() );
  /*  we haven't moved the cursor forward, if we call scan again it won't 
      do anything  */
  CPPUNIT_ASSERT ( scanner.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + FIRST_PIH );
  CPPUNIT_ASSERT ( data_ + SCAN_DATA_SIZE - pih == scanner.sizeFwd() );
  /* now let's move the cursor forward  */
  pih = data_ + SECOND_PIH;
  CPPUNIT_ASSERT ( scanner.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + SECOND_PIH );
  CPPUNIT_ASSERT ( data_ + SCAN_DATA_SIZE - pih == scanner.sizeFwd() );

  ++pih;
  CPPUNIT_ASSERT ( scanner.scanForward() );
  CPPUNIT_ASSERT ( pih == data_ + THIRD_PIH );
  CPPUNIT_ASSERT ( data_ + SCAN_DATA_SIZE - pih == scanner.sizeFwd() );
}

void 
ScannerTest::testSizeBwd()
{
  uint8 const* pih = data_ + THIRD_PIH + 100;
  Scanner scanner ( pih, data_ + SCAN_DATA_SIZE - pih, pih - data_ );
  CPPUNIT_ASSERT ( scanner.scanBackward() );
  CPPUNIT_ASSERT ( pih == data_ + SECOND_PIH );
  CPPUNIT_ASSERT ( pih - data_ == scanner.sizeBwd() );
  
  CPPUNIT_ASSERT ( scanner.scanBackward() );
  CPPUNIT_ASSERT ( pih == data_ + FIRST_PIH );
  CPPUNIT_ASSERT ( pih - data_ == scanner.sizeBwd() );
  
  CPPUNIT_ASSERT ( !scanner.scanBackward() );
  CPPUNIT_ASSERT ( pih == data_ + 3 );
  CPPUNIT_ASSERT ( pih - data_ == scanner.sizeBwd() );
  
  pih = data_ + THIRD_PIH;
  CPPUNIT_ASSERT ( scanner.scanBackward() );
  CPPUNIT_ASSERT ( pih == data_ + SECOND_PIH );
  CPPUNIT_ASSERT ( pih - data_ == scanner.sizeBwd() );      
}

void 
ScannerTest::fillBuffer ( uint8* buffer, uint8 parseCode
                        , uint32 next, uint32 prev)
{
  *buffer++ = 0x42;
  *buffer++ = 0x42;
  *buffer++ = 0x43;
  *buffer++ = 0x44;
  *buffer++ = parseCode;
  write4Bytes ( buffer, next );
  write4Bytes ( buffer + 4, prev );
}

void 
ScannerTest::write4Bytes ( uint8* buffer, uint32 data )
{
  *buffer++ = static_cast<uint8> ( ( data & 0xFF000000 ) >> 24 );
  *buffer++ = static_cast<uint8> ( ( data & 0x00FF0000 ) >> 16 );
  *buffer++ = static_cast<uint8> ( ( data & 0x0000FF00 ) >> 8 );
  *buffer   = static_cast<uint8> ( data & 0x000000FF );
}

