/* ***** BEGIN LICENSE BLOCK *****
*
* $Id:  $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unittest/AccessUnitTest.hpp>
#include <unittest/core_suite.hpp>

#include <parser/AccessUnit.hpp>

using ::dirac::parser_project::AccessUnit;

//NOTE: ensure that the suite is added to the default registry in
//cppunit_testsuite.cpp
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION (AccessUnitTest, coreSuiteName());

AccessUnitTest::AccessUnitTest() {}

AccessUnitTest::~AccessUnitTest() {}

void 
AccessUnitTest::setUp()
{
  data_ = new uint8[ARRAY_SIZE];
  memset ( static_cast<void*> ( data_ ), 0, ARRAY_SIZE );
  
  fillBuffer  ( data_ + SEQHDR_START, SEQUENCE_HEADER
              , SEQHDR_NEXT, SEQHDR_PREV );
  fillBuffer  ( data_ + PIC1_START, INTRA_REF_AC, PIC1_NEXT, PIC1_PREV );
  write4Bytes ( data_ + PIC1_START + 13, 0 );
  fillBuffer  ( data_ + PIC2_START, INTER_REF_AC_1REF, PIC2_NEXT, PIC2_PREV );
  write4Bytes ( data_ + PIC2_START + 13, 1 );
  fillBuffer  ( data_ + PAD_START, PADDING_DATA, PAD_NEXT, PAD_PREV );
  fillBuffer  ( data_ + PIC3_START, INTER_NON_REF_AC_1REF
              , PIC3_NEXT, PIC3_PREV );
  write4Bytes ( data_ + PIC3_START + 13, 2 );
  fillBuffer  ( data_ + AUX_START, AUX_DATA, AUX_NEXT, AUX_PREV );
  fillBuffer  ( data_ + EOS_START, END_OF_SEQUENCE, EOS_NEXT, EOS_PREV );
}

void 
AccessUnitTest::tearDown()
{
  delete [] data_;
}

void 
AccessUnitTest::testAccessUnit()
{
  uint8* buffer = data_ + SEQHDR_START;
  CPPUNIT_ASSERT ( AccessUnit ( buffer, SEQHDR_NEXT ).isSequenceHeader() );
  CPPUNIT_ASSERT_EQUAL  ( AccessUnit ( buffer, SEQHDR_NEXT ).size()
                    , static_cast<size_t> ( SEQHDR_NEXT ) );
  AccessUnit au;
  au.setp ( data_ + PIC1_START, PIC1_NEXT );
  CPPUNIT_ASSERT ( !au.empty() );
  CPPUNIT_ASSERT_EQUAL ( au.size(), static_cast<size_t> ( PIC1_NEXT ) );
  CPPUNIT_ASSERT ( au.isPicture() );
  CPPUNIT_ASSERT ( au.isIPicture() );
  std::auto_ptr<AccessUnit::Buffer> buf ( au.releaseBuffer() );
  CPPUNIT_ASSERT_EQUAL ( buf->size(), static_cast<size_t> ( PIC1_NEXT ) );
  au.setp ( data_ + PAD_START, PAD_NEXT );
  CPPUNIT_ASSERT ( !au.isAuxiliaryData() );
  CPPUNIT_ASSERT ( au.isPaddingData() );
  CPPUNIT_ASSERT ( AccessUnit ( data_ + EOS_START, 13 ).isEndOfSequence() );
  CPPUNIT_ASSERT ( !AccessUnit ( data_ + EOS_START, 13 ).isSequenceHeader() );
}

void 
AccessUnitTest::fillBuffer  ( uint8* buffer, uint8 parseCode
                            , uint32 next, uint32 prev)
{
  *buffer++ = 0x42;
  *buffer++ = 0x42;
  *buffer++ = 0x43;
  *buffer++ = 0x44;
  *buffer++ = parseCode;
  write4Bytes ( buffer, next );
  write4Bytes ( buffer + 4, prev );
}

void 
AccessUnitTest::write4Bytes ( uint8* buffer, uint32 data )
{
  *buffer++ = static_cast<uint8> ( ( data & 0xFF000000 ) >> 24 );
  *buffer++ = static_cast<uint8> ( ( data & 0x00FF0000 ) >> 16 );
  *buffer++ = static_cast<uint8> ( ( data & 0x0000FF00 ) >> 8 );
  *buffer   = static_cast<uint8> ( data & 0x000000FF );
}


