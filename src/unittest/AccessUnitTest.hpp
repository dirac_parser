/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: AccessUnitTestSuite.hpp 40 2008-02-10 18:02:04Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _ACCESSUNITTEST_HPP_
#define _ACCESSUNITTEST_HPP_

// #include <cxxtest/TestSuite.h>

#include <cppunit/extensions/HelperMacros.h>

#include <common/dirac.h>

class AccessUnitTest : public CPPUNIT_NS::TestFixture
{
  CPPUNIT_TEST_SUITE ( AccessUnitTest );
  CPPUNIT_TEST ( testAccessUnit );
  CPPUNIT_TEST_SUITE_END();
  
public:
  AccessUnitTest();
  virtual ~AccessUnitTest();
  
  virtual void setUp();
  virtual void tearDown();
  
  void testAccessUnit();
    
private:
  void fillBuffer ( uint8* buffer, uint8 parseCode, uint32 next, uint32 prev );
  void write4Bytes ( uint8* buffer, uint32 data );
  
  enum {
      ARRAY_SIZE      = 10000
    , SEQHDR_START    = 2
    , SEQHDR_NEXT     = 98
    , SEQHDR_PREV     = 0
    , PIC1_START      = 100
    , PIC1_NEXT       = 500
    , PIC1_PREV       = 100
    , PIC2_START      = 600
    , PIC2_NEXT       = 600
    , PIC2_PREV       = 500
    , PAD_START       = 1200
    , PAD_NEXT        = 50
    , PAD_PREV        = 600
    , PIC3_START      = 1250
    , PIC3_NEXT       = 450
    , PIC3_PREV       = 50
    , AUX_START       = 1700
    , AUX_NEXT        = 200
    , AUX_PREV        = 450
    , EOS_START       = 1900
    , EOS_NEXT        = 0
    , EOS_PREV        = 200
  };
  
  /*  Based on DiracSpec1.0.0_pre11, 20 Dec 2007  */
  enum Parsecodes {
      SEQUENCE_HEADER         = 0x00
    , END_OF_SEQUENCE         = 0x10
    , AUX_DATA                = 0x20
    , PADDING_DATA            = 0x30
    , INTRA_REF_AC            = 0x0C
    , INTRA_NONREF_AC         = 0x08
    , INTRA_REF_NOAC          = 0x4C
    , INTRA_NONREF_NOAC       = 0x48
    , INTER_REF_AC_1REF       = 0x0D
    , INTER_REF_AC_2REF       = 0x0E
    , INTER_NON_REF_AC_1REF   = 0x09
    , INTER_NON_REF_AC_2REF   = 0x0A
    /* low delay syntax */
    , INTRA_REF_LD            = 0xCC
    , INTRA_NON_REF_LD        = 0xC8
  };
  
  uint8* data_;
};

#endif /* _ACCESSUNITTEST_HPP_ */
