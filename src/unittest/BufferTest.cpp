/* ***** BEGIN LICENSE BLOCK *****
*
* $Id:  $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unittest/BufferTest.hpp>

#include <unittest/core_suite.hpp>

#include <cstring>


//NOTE: ensure that the suite is added to the default registry in
//cppunit_testsuite.cpp
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION (BufferTest, coreSuiteName());

BufferTest::BufferTest() {}

BufferTest::~BufferTest() {}

void 
BufferTest::setUp() 
{
  data_ = new uint8[ARRAY_SIZE];
  memset ( static_cast<void*> ( data_ ), 0, ARRAY_SIZE );
  cursor0_ = data_;
  cursor1_ = data_ + CURSOR_POS;
  cursor2_ = data_ + ARRAY_SIZE;

  bufferConst = new Buffer ( cursor0_, ARRAY_SIZE );
  buffer = new Buffer ( cursor0_, ARRAY_SIZE );
  bufferConstWithCursor = new Buffer  ( cursor1_
                                      , ARRAY_SIZE - CURSOR_POS
                                      , CURSOR_POS);
  bufferWithCursor = new Buffer ( cursor1_
                                , ARRAY_SIZE - CURSOR_POS
                                , CURSOR_POS );
  emptyBuffer = new Buffer ( cursor2_, 0 );
  emptyConstBuffer = new Buffer ( cursor2_, 0 );
  emptyFwdBuffer = new Buffer ( cursor2_, 0, ARRAY_SIZE );
  emptyBwdBuffer = new Buffer ( cursor0_, ARRAY_SIZE, 0 );
}

void 
BufferTest::tearDown()
{
  delete emptyBwdBuffer;
  delete emptyFwdBuffer;
  delete emptyConstBuffer;
  delete emptyBuffer;
  delete bufferWithCursor;
  delete bufferConstWithCursor;
  delete buffer;
  delete bufferConst;
  
  delete [] data_;
}

void 
BufferTest::testBegin()
{
  CPPUNIT_ASSERT ( buffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ )
                        , buffer->begin() );
  CPPUNIT_ASSERT ( bufferConst );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ )
                        , bufferConst->begin() );
  
  CPPUNIT_ASSERT ( bufferWithCursor );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ )
                        , bufferWithCursor->begin() );
  CPPUNIT_ASSERT ( bufferConstWithCursor );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ )
                        , bufferConstWithCursor->begin() );
  
  CPPUNIT_ASSERT ( emptyBuffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , emptyBuffer->begin() );
  CPPUNIT_ASSERT ( emptyConstBuffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , emptyConstBuffer->begin() );
  
  CPPUNIT_ASSERT ( emptyFwdBuffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ )
                        , emptyFwdBuffer->begin() );
  CPPUNIT_ASSERT ( emptyBwdBuffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ )
                        , emptyBwdBuffer->begin() );
}

void 
BufferTest::testEnd()
{
  CPPUNIT_ASSERT ( buffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , buffer->end() );
  CPPUNIT_ASSERT ( bufferConst );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , bufferConst->end() );
  
  CPPUNIT_ASSERT ( bufferWithCursor );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , bufferWithCursor->end() );
  CPPUNIT_ASSERT ( bufferConstWithCursor );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , bufferConstWithCursor->end() );
  
  CPPUNIT_ASSERT ( emptyBuffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , emptyBuffer->end() );
  CPPUNIT_ASSERT ( emptyConstBuffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , emptyConstBuffer->end() );
  
  CPPUNIT_ASSERT ( emptyFwdBuffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , emptyFwdBuffer->end() );
  CPPUNIT_ASSERT (emptyBwdBuffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , emptyBwdBuffer->end() );
}

void 
BufferTest::testSize()
{
  CPPUNIT_ASSERT ( buffer );
  CPPUNIT_ASSERT_EQUAL ( static_cast<uint32> ( ARRAY_SIZE ), buffer->size() );
  CPPUNIT_ASSERT ( bufferConst );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint32> ( ARRAY_SIZE )
                    , bufferConst->size() );
  
  CPPUNIT_ASSERT ( bufferWithCursor );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint32> ( ARRAY_SIZE )
                    , bufferWithCursor->size() );
  CPPUNIT_ASSERT ( bufferConstWithCursor );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint32> ( ARRAY_SIZE )
                    , bufferConstWithCursor->size() );
  
  CPPUNIT_ASSERT ( emptyBuffer );
  CPPUNIT_ASSERT_EQUAL ( static_cast<uint32> ( 0 ), emptyBuffer->size() );
  CPPUNIT_ASSERT (emptyConstBuffer );
  CPPUNIT_ASSERT_EQUAL ( static_cast<uint32> ( 0 ), emptyConstBuffer->size() );
  
  CPPUNIT_ASSERT ( emptyFwdBuffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint32> ( ARRAY_SIZE )
                    , emptyFwdBuffer->size() );
  CPPUNIT_ASSERT ( emptyBwdBuffer );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint32> ( ARRAY_SIZE )
                    , emptyBwdBuffer->size() );
}


void 
BufferTest::testEmpty()
{
  CPPUNIT_ASSERT ( buffer );
  CPPUNIT_ASSERT_EQUAL ( false, buffer->empty() );
  CPPUNIT_ASSERT ( bufferConst );
  CPPUNIT_ASSERT_EQUAL  ( false, bufferConst->empty() );
  
  CPPUNIT_ASSERT ( bufferWithCursor );
  CPPUNIT_ASSERT_EQUAL ( false, bufferWithCursor->empty() );
  CPPUNIT_ASSERT ( bufferConstWithCursor );
  CPPUNIT_ASSERT_EQUAL  ( false, bufferConstWithCursor->empty() );
  
  CPPUNIT_ASSERT ( emptyBuffer && emptyBuffer->empty() );
  CPPUNIT_ASSERT ( emptyConstBuffer && emptyConstBuffer->empty() );
  
  CPPUNIT_ASSERT ( emptyFwdBuffer );
  CPPUNIT_ASSERT_EQUAL ( false, emptyFwdBuffer->empty() );
  CPPUNIT_ASSERT ( emptyBwdBuffer );
  CPPUNIT_ASSERT_EQUAL ( false, emptyBwdBuffer->empty() );
}

void 
BufferTest::testRbegin()
{
  typedef std::reverse_iterator<uint8 const*> const_reverse_iterator;
  
  CPPUNIT_ASSERT ( buffer );
  CPPUNIT_ASSERT (  const_reverse_iterator ( data_ + ARRAY_SIZE ) 
                 == buffer->rbegin() );
  CPPUNIT_ASSERT ( bufferConst );
  CPPUNIT_ASSERT  (  const_reverse_iterator ( data_ + ARRAY_SIZE )
                  == bufferConst->rbegin() );
        
  CPPUNIT_ASSERT ( bufferWithCursor );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ + ARRAY_SIZE)
                == bufferWithCursor->rbegin() );
  CPPUNIT_ASSERT ( bufferConstWithCursor );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ + ARRAY_SIZE )
                 == bufferConstWithCursor->rbegin() );
  
  CPPUNIT_ASSERT ( emptyBuffer );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ + ARRAY_SIZE )
                 == emptyBuffer->rbegin() );
  CPPUNIT_ASSERT ( emptyConstBuffer );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ + ARRAY_SIZE )
                 == emptyConstBuffer->rbegin() );
                  
  CPPUNIT_ASSERT ( emptyFwdBuffer );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ + ARRAY_SIZE )
                 == emptyFwdBuffer->rbegin() );
  CPPUNIT_ASSERT ( emptyBwdBuffer );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ + ARRAY_SIZE )
                 == emptyBwdBuffer->rbegin() );
}

void 
BufferTest::testRend()
{
  typedef std::reverse_iterator<uint8 const*> const_reverse_iterator;
  
  CPPUNIT_ASSERT ( buffer );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ ) == buffer->rend() );
  CPPUNIT_ASSERT ( bufferConst );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_) == bufferConst->rend() );
  
  CPPUNIT_ASSERT ( bufferWithCursor );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_)
                 == bufferWithCursor->rend() );
  CPPUNIT_ASSERT ( bufferConstWithCursor );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ )
                 == bufferConstWithCursor->rend() );
                    
  CPPUNIT_ASSERT ( emptyBuffer );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ + ARRAY_SIZE )
                 == emptyBuffer->rend() );
  CPPUNIT_ASSERT ( emptyConstBuffer );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ + ARRAY_SIZE )
                 == emptyConstBuffer->rend() );
                    
  CPPUNIT_ASSERT ( emptyFwdBuffer );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ )
                == emptyFwdBuffer->rend() );
  CPPUNIT_ASSERT ( emptyBwdBuffer );
  CPPUNIT_ASSERT ( const_reverse_iterator ( data_ )
                 == emptyBwdBuffer->rend() );
}

void 
BufferTest::testCursor()
{
  CPPUNIT_ASSERT ( buffer );
  CPPUNIT_ASSERT ( bufferWithCursor );
  CPPUNIT_ASSERT ( bufferConst );
  CPPUNIT_ASSERT ( bufferConstWithCursor );
  CPPUNIT_ASSERT ( emptyBuffer );
  CPPUNIT_ASSERT ( emptyConstBuffer );
  CPPUNIT_ASSERT ( emptyFwdBuffer );
  CPPUNIT_ASSERT ( emptyBwdBuffer );

  // rvalue use
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ )
                        , static_cast<uint8 const*> ( buffer->cursor() ) );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ )
                        , static_cast<uint8 const*> ( bufferConst->cursor() ) );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + CURSOR_POS
                        , static_cast<uint8 const*> ( bufferConstWithCursor->cursor() ) );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + CURSOR_POS
                        , static_cast<uint8 const*> ( bufferWithCursor->cursor() ) );
  
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , static_cast<uint8 const*> ( emptyBuffer->cursor() ) );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , static_cast<uint8 const*> ( emptyConstBuffer->cursor() ) );
  
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + ARRAY_SIZE
                        , static_cast<uint8 const*> ( emptyFwdBuffer->cursor() ) );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ )
                        , static_cast<uint8 const*> ( emptyBwdBuffer->cursor() ) );
  
  // lvalue uses
  ++buffer->cursor();
  CPPUNIT_ASSERT_EQUAL  ( cursor0_
                        , static_cast<uint8 const*> ( buffer->cursor() ) );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + 1
                        , static_cast<uint8 const*> ( buffer->cursor() ) );
  buffer->cursor() = data_;
  CPPUNIT_ASSERT_EQUAL  ( cursor0_, static_cast<uint8 const*> ( data_ ) );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ )
                        , static_cast<uint8 const*> ( buffer->cursor() ) );
  
  ++bufferWithCursor->cursor();
  CPPUNIT_ASSERT_EQUAL  ( cursor1_
                        , static_cast<uint8 const*> ( bufferWithCursor->cursor() ) );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + 1 + CURSOR_POS
                        , static_cast<uint8 const*> ( bufferWithCursor->cursor() ) );
  bufferWithCursor->cursor() = data_ + CURSOR_POS;
  CPPUNIT_ASSERT_EQUAL  ( cursor1_
                        , static_cast<uint8 const*> ( bufferWithCursor->cursor() ) );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + CURSOR_POS
                        , static_cast<uint8 const*> (bufferWithCursor->cursor() ) );
  
  ++emptyBwdBuffer->cursor();
  CPPUNIT_ASSERT_EQUAL  ( cursor0_
                        , static_cast<uint8 const*> ( emptyBwdBuffer->cursor() ) );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ ) + 1
                        , static_cast<uint8 const*> ( emptyBwdBuffer->cursor() ) );
  emptyBwdBuffer->cursor() = data_;
  CPPUNIT_ASSERT_EQUAL  ( cursor0_
                        , static_cast<uint8 const*> ( emptyBwdBuffer->cursor() ) );
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8 const*> ( data_ )
                        , static_cast<uint8 const*> ( emptyBwdBuffer->cursor() ) );
}

