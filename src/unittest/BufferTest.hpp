/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: BufferTestSuite.hpp 37 2008-02-07 09:18:30Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _BUFFERTEST_HPP_
#define _BUFFERTEST_HPP_

#include <cppunit/extensions/HelperMacros.h>

#include <common/dirac.h>
#include <parser/buffer.hpp>


using dirac::parser_project::Buffer;

class BufferTest : public CPPUNIT_NS::TestFixture
{
  CPPUNIT_TEST_SUITE ( BufferTest );
  CPPUNIT_TEST ( testBegin );
  CPPUNIT_TEST ( testEnd );
  CPPUNIT_TEST ( testSize );
  CPPUNIT_TEST ( testEmpty );
  CPPUNIT_TEST ( testRbegin );
  CPPUNIT_TEST ( testRend );
  CPPUNIT_TEST ( testCursor );
  CPPUNIT_TEST_SUITE_END();
  
public:
  BufferTest();
  virtual ~BufferTest();
  
  virtual void setUp();
  virtual void tearDown();
  
  void testBegin();
  void testEnd();
  void testSize();
  void testEmpty();
  void testRbegin();
  void testRend();
  void testCursor();
  
  
private:

  enum {
    ARRAY_SIZE = 1000
  , CURSOR_POS = 100
  };

  uint8*              data_;
  uint8 const*        cursor0_;
  uint8 const*        cursor1_;
  uint8 const*        cursor2_;
  Buffer const*       bufferConst;
  Buffer*             buffer;
  Buffer const*       bufferConstWithCursor;
  Buffer*             bufferWithCursor;
  Buffer*             emptyBuffer;
  Buffer const*       emptyConstBuffer;
  Buffer             *emptyFwdBuffer, *emptyBwdBuffer;
  Buffer const*       emptyConstFwdBuffer;
  Buffer const*       emptyConstBwdBuffer;
};


#endif /* _BUFFERTEST_HPP_ */
