/* ***** BEGIN LICENSE BLOCK *****
*
* $Id:  $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unittest/StreamReaderTest.hpp>
#include <unittest/core_suite.hpp>

#include <parser/StreamReader.hpp>
#include <parser/IInput.hpp>

#include <decoder/FileReader.hpp>


#include <memory>

using namespace ::dirac::parser_project;
using namespace ::dirac::decoder;

//NOTE: ensure that the suite is added to the default registry in
//cppunit_testsuite.cpp
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION (StreamReaderTest, coreSuiteName());

#ifdef BITS
std::string StreamReaderTest::filename_ = BITS;
#else
std::string StreamReaderTest::filename_ = "../bitstreams/AN_schro_1Mbps.drc";
#endif

StreamReaderTest::StreamReaderTest() {}

StreamReaderTest::~StreamReaderTest() {}

void 
StreamReaderTest::setUp() {}

void 
StreamReaderTest::tearDown() {}

void 
StreamReaderTest::testStreamReader()
{
  std::auto_ptr<IInput> input ( new FileReader ( filename_ ) );
  CPPUNIT_ASSERT ( input->isAvailable() );
  // it's important that the size of the buffer be smaller than the test
  // file or else we can't test adding data backward
  const size_t STREAM_SIZE = 2; /* 2 * 4096 * 10 */
  StreamReader str ( input, STREAM_SIZE );
  CPPUNIT_ASSERT_EQUAL ( static_cast<int64> ( 0 ), str.goffset() );
  uint8 const*& gptr = str.gptr();
  CPPUNIT_ASSERT_EQUAL ( gptr, str.eback() );
  const size_t OFFSET = 1000;
  gptr += OFFSET;
  CPPUNIT_ASSERT_EQUAL ( str.gptr(), str.eback() + OFFSET );
  CPPUNIT_ASSERT_EQUAL  ( str.size(), str.sizeBwd() + str.sizeFwd() );
  CPPUNIT_ASSERT_EQUAL  ( str.sizeBwd(), OFFSET );
  gptr = str.eback() + 300;
  CPPUNIT_ASSERT ( str.addDataBwd() );
  CPPUNIT_ASSERT_EQUAL  ( gptr, str.egptr() );
  CPPUNIT_ASSERT_EQUAL ( static_cast<int64> ( 300 ), str.goffset() );
}
