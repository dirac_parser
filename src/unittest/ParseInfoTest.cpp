/* ***** BEGIN LICENSE BLOCK *****
*
* $Id:  $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <unittest/ParseInfoTest.hpp>
#include <unittest/core_suite.hpp>

#include <parser/parse_info.hpp>

#include <cstring>

using ::dirac::parser_project::ParseInfo;

//NOTE: ensure that the suite is added to the default registry in
//cppunit_testsuite.cpp
CPPUNIT_TEST_SUITE_NAMED_REGISTRATION (ParseInfoTest, coreSuiteName());


#define FILL_SEQUENCE_HEADER    fillBuffer ( buffer, SEQUENCE_HEADER, 120, 0 )
#define FILL_END_OF_SEQUENCE    fillBuffer ( buffer, END_OF_SEQUENCE, 13, 0 )
#define FILL_AUXILIARY_DATA     fillBuffer ( buffer, AUX_DATA, 120, 0 )
#define FILL_PADDING_DATA       fillBuffer ( buffer, PADDING_DATA, 200, 0 )
#define FILL_INTRA_REF0_AC      fillBuffer ( buffer, INTRA_REF_AC, 300, 0 )
#define FILL_INTRA_NONREF_AC    fillBuffer ( buffer, INTRA_NONREF_AC, 300, 0 )
#define FILL_INTRA_REF_NOAC     fillBuffer ( buffer, INTRA_REF_NOAC, 300, 0 )
#define FILL_INTRA_NONREF_NOAC  fillBuffer ( buffer, INTRA_NONREF_NOAC, 300, 0 )
#define FILL_INTER_REF1_AC      fillBuffer ( buffer, INTER_REF_AC_1REF, 300, 0 )
#define FILL_INTER_REF2_AC      fillBuffer ( buffer, INTER_REF_AC_2REF, 300, 0 )
#define FILL_INTER_NOREF_AC_1R  fillBuffer ( buffer, INTER_NON_REF_AC_1REF, 300, 0 )
#define FILL_INTER_NOREF_AC_2R  fillBuffer ( buffer, INTER_NON_REF_AC_2REF, 300, 0 )
#define FILL_INTRA_REF_LD       fillBuffer ( buffer, INTRA_REF_LD, 500, 0 )
#define FILL_INTRA_NOREF_LD     fillBuffer ( buffer, INTRA_NON_REF_LD, 500, 0 )


ParseInfoTest::ParseInfoTest() {}

ParseInfoTest::~ParseInfoTest() {}

void 
ParseInfoTest::setUp()
{
  data_ = new uint8 [DATA_SIZE];
  memset ( static_cast<void*> ( data_ ), 0, DATA_SIZE );
  buffer = data_ + FIRST_PIH;
}

void 
ParseInfoTest::tearDown()
{
  delete [] data_;
}

void 
ParseInfoTest::testIsParseInfo() // test here all static methods
{
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( ParseInfo::isParseInfo ( buffer, DATA_SIZE ) );
  CPPUNIT_ASSERT ( !ParseInfo::isParseInfo ( buffer, 12 ) );
  CPPUNIT_ASSERT ( !ParseInfo::isParseInfo ( buffer + 1, 100 ) );
  
  CPPUNIT_ASSERT_EQUAL ( buffer[0], *ParseInfo::prefixBegin() );
  CPPUNIT_ASSERT_EQUAL ( buffer[3], *( ParseInfo::prefixEnd() - 1 ) );
  
  CPPUNIT_ASSERT_EQUAL ( buffer[3], *( ParseInfo::prefixRbegin().base() - 1 ) );
  CPPUNIT_ASSERT_EQUAL ( buffer[0], *( ParseInfo::prefixRend().base() ) );
  CPPUNIT_ASSERT_EQUAL ( buffer[1], *( ParseInfo::prefixRend().base() + 1 ) );
  CPPUNIT_ASSERT_EQUAL ( buffer[2], *( ParseInfo::prefixRend().base() + 2 ) );
  
  
  CPPUNIT_ASSERT_EQUAL ( static_cast<uint32> ( 4 ), ParseInfo::prefixSize() );
}

void 
ParseInfoTest::testIsSequenceHeader()
{
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( ParseInfo ( buffer).isSequenceHeader() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer).isSequenceHeader() );
  fillBuffer ( buffer, SEQUENCE_HEADER + 1, 128, 0 );
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isSequenceHeader() );
}

void 
ParseInfoTest::testIsEndOfSequence()
{
  FILL_END_OF_SEQUENCE;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isEndOfSequence() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isEndOfSequence() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isEndOfSequence() );
  fillBuffer ( buffer, END_OF_SEQUENCE - 1, 129, 0 );
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isEndOfSequence() );
}

void 
ParseInfoTest::testIsAuxiliaryData()
{
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isAuxiliaryData() );
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isAuxiliaryData() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isAuxiliaryData() );

  // now check range: current specs allows for future extensions to aux data
  // params, from 0x20 to 0x27
  fillBuffer ( data_ + FIRST_PIH, 0x26, 128, 0 );
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isAuxiliaryData() );
  fillBuffer ( data_ + FIRST_PIH, 0x28, 128, 0 );
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isAuxiliaryData() );
}

void 
ParseInfoTest::testIsPaddingData()
{
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isPaddingData() );
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isPaddingData() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isPaddingData() );

  fillBuffer ( buffer, PADDING_DATA - 1, 129, 0 );
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isPaddingData() );
}

void 
ParseInfoTest::testIsPicture()
{     
  FILL_INTRA_REF0_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isPicture() );
  FILL_INTRA_NONREF_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isPicture() );
  FILL_INTRA_REF_NOAC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isPicture() );
  FILL_INTRA_NONREF_NOAC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isPicture() );
  FILL_INTER_REF1_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isPicture() );
  FILL_INTER_REF2_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isPicture() );
  FILL_INTER_NOREF_AC_1R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isPicture() );
  FILL_INTER_NOREF_AC_2R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isPicture() );
  FILL_INTRA_REF_LD;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isPicture() );
  FILL_INTRA_NOREF_LD;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isPicture() );
        
  fillBuffer ( buffer, 0x07, 300, 0 );
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isPicture() );
  fillBuffer ( buffer, 0x86, 300, 0 );
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isPicture() );
  
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isPicture() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isPicture() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isPicture() );
  FILL_END_OF_SEQUENCE;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isPicture() );
}

void 
ParseInfoTest::testIsCoreSyntax()
{
  FILL_INTRA_REF0_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isCoreSyntax() );
  FILL_INTRA_NONREF_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isCoreSyntax() );
  FILL_INTRA_REF_NOAC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isCoreSyntax() );
  FILL_INTRA_NONREF_NOAC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isCoreSyntax() );
  FILL_INTER_REF1_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isCoreSyntax() );
  FILL_INTER_REF2_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isCoreSyntax() );
  FILL_INTER_NOREF_AC_1R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isCoreSyntax() );
  FILL_INTER_NOREF_AC_2R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isCoreSyntax() );
  
  FILL_INTRA_REF_LD;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isCoreSyntax() );
  FILL_INTRA_NOREF_LD;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isCoreSyntax() );
  
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isCoreSyntax() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isCoreSyntax() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isCoreSyntax() );
  FILL_END_OF_SEQUENCE;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isCoreSyntax() );

  fillBuffer ( buffer, 0x88, 100, 0 ); // perhaps not a valid code
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isCoreSyntax() );
}

void 
ParseInfoTest::testIsLowDelaySyntax()
{
  FILL_INTRA_REF_LD;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isLowDelaySyntax() );
  FILL_INTRA_NOREF_LD;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isLowDelaySyntax() );
  fillBuffer ( buffer, 0x88, 100, 0 ); // this is not valid at the moment
  // but it is passed as a low delay code
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isLowDelaySyntax() );

  FILL_INTRA_REF0_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );
  FILL_INTRA_NONREF_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );
  FILL_INTRA_REF_NOAC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );
  FILL_INTRA_NONREF_NOAC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );
  FILL_INTER_REF1_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );
  FILL_INTER_REF2_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );
  FILL_INTER_NOREF_AC_1R;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );
  FILL_INTER_NOREF_AC_2R;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );
  
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );
  FILL_END_OF_SEQUENCE;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isLowDelaySyntax() );      
}

void 
ParseInfoTest::testIsUsingArithmeticCoding()
{
  FILL_INTRA_REF0_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isUsingArithmeticCoding() );
  FILL_INTRA_NONREF_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isUsingArithmeticCoding() );
  FILL_INTER_REF1_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isUsingArithmeticCoding() );
  FILL_INTER_REF2_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isUsingArithmeticCoding() );
  FILL_INTER_NOREF_AC_1R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isUsingArithmeticCoding() );
  FILL_INTER_NOREF_AC_2R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isUsingArithmeticCoding() );
  
  FILL_INTRA_REF_NOAC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isUsingArithmeticCoding() );
  FILL_INTRA_NONREF_NOAC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isUsingArithmeticCoding() );
  FILL_INTRA_REF_LD;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isUsingArithmeticCoding() );
  FILL_INTRA_NOREF_LD;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isUsingArithmeticCoding() );
  
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isUsingArithmeticCoding() );
  FILL_END_OF_SEQUENCE;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isUsingArithmeticCoding() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isUsingArithmeticCoding() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isUsingArithmeticCoding() );
}

void 
ParseInfoTest::testIsReference()
{
  FILL_INTRA_REF0_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isReference() );
  FILL_INTRA_REF_NOAC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isReference() );
  FILL_INTER_REF1_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isReference() );
  FILL_INTER_REF2_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isReference() );
  FILL_INTRA_REF_LD;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isReference() );
  
  FILL_INTRA_NONREF_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isReference() );
  FILL_INTRA_NONREF_NOAC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isReference() );
  FILL_INTER_NOREF_AC_1R;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isReference() );
  FILL_INTER_NOREF_AC_2R;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isReference() );
  FILL_INTRA_NOREF_LD;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isReference() );
  
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isReference() );
  FILL_END_OF_SEQUENCE;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isReference() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isReference() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isReference() );
}

void 
ParseInfoTest::testIsNotReference()
{
  FILL_INTRA_NONREF_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isNotReference() );
  FILL_INTRA_NONREF_NOAC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isNotReference() );
  FILL_INTER_NOREF_AC_1R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isNotReference() );
  FILL_INTER_NOREF_AC_2R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isNotReference() );
  FILL_INTRA_NOREF_LD;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isNotReference() );
  
  FILL_INTRA_REF0_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isNotReference() );
  FILL_INTRA_REF_NOAC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isNotReference() );
  FILL_INTER_REF1_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isNotReference() );
  FILL_INTER_REF2_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isNotReference() );
  FILL_INTRA_REF_LD;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isNotReference() );
  
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isNotReference() );
  FILL_END_OF_SEQUENCE;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isNotReference() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isNotReference() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isNotReference() );
}

void 
ParseInfoTest::testIsIntra()
{
  FILL_INTRA_REF0_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isIntra() );
  FILL_INTRA_NONREF_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isIntra() );
  FILL_INTRA_REF_NOAC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isIntra() );
  FILL_INTRA_NONREF_NOAC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isIntra() );
  FILL_INTRA_REF_LD;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isIntra() );
  FILL_INTRA_NOREF_LD;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isIntra() );
  
  FILL_INTER_REF1_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isIntra() );
  FILL_INTER_REF2_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isIntra() );
  FILL_INTER_NOREF_AC_1R;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isIntra() );
  FILL_INTER_NOREF_AC_2R;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isIntra() );
  
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isIntra() );
  FILL_END_OF_SEQUENCE;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isIntra() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isIntra() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isIntra() );
}

void 
ParseInfoTest::testIsInter()
{
  FILL_INTER_REF1_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isInter() );
  FILL_INTER_REF2_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isInter() );
  FILL_INTER_NOREF_AC_1R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isInter() );
  FILL_INTER_NOREF_AC_2R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).isInter() );
  
  FILL_INTRA_REF0_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isInter() );
  FILL_INTRA_NONREF_AC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isInter() );
  FILL_INTRA_REF_NOAC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isInter() );
  FILL_INTRA_NONREF_NOAC;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isInter() );
  
  FILL_INTRA_REF_LD;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isInter() );
  FILL_INTRA_NOREF_LD;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isInter() );
  
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isInter() );
  FILL_END_OF_SEQUENCE;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isInter() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isInter() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).isInter() );
}

void 
ParseInfoTest::testGetNumberOfReferences()
{
  FILL_INTRA_REF0_AC;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 0 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );
  FILL_INTER_REF1_AC;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 1 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );
  FILL_INTER_REF2_AC;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 2 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );
  FILL_INTER_NOREF_AC_1R;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 1 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );
  FILL_INTER_NOREF_AC_2R;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 2 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );

  FILL_INTRA_REF_LD;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 0 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );
  FILL_INTRA_NOREF_LD;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 0 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );
  FILL_INTRA_NONREF_AC;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 0 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 0 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 0 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 0 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );
  FILL_END_OF_SEQUENCE;
  CPPUNIT_ASSERT_EQUAL  ( static_cast<uint8> ( 0 )
                    , ParseInfo ( buffer ).getNumberOfReferences() );
}

void 
ParseInfoTest::testHasPayload()
{
  FILL_SEQUENCE_HEADER;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_AUXILIARY_DATA;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_PADDING_DATA;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_INTRA_REF0_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_INTRA_NONREF_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_INTRA_REF_NOAC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_INTRA_NONREF_NOAC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_INTER_REF1_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_INTER_REF2_AC;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_INTER_NOREF_AC_1R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_INTER_NOREF_AC_2R;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_INTRA_REF_LD;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  FILL_INTRA_NOREF_LD;
  CPPUNIT_ASSERT ( ParseInfo ( buffer ).hasPayload() );
  
  FILL_END_OF_SEQUENCE;
  CPPUNIT_ASSERT ( !ParseInfo ( buffer ).hasPayload() );
}

void 
ParseInfoTest::testNext()
{
  fillBuffer ( buffer, SEQUENCE_HEADER, 100, 0 );
  CPPUNIT_ASSERT ( buffer + 100 == ParseInfo ( buffer ).next() );
  fillBuffer ( buffer, AUX_DATA, 300, 0 );
  CPPUNIT_ASSERT ( buffer + 300 == ParseInfo ( buffer ).next() );
  fillBuffer ( buffer, PADDING_DATA, 70000, 0);
                    
  fillBuffer ( buffer, END_OF_SEQUENCE, 0, 100 );
  CPPUNIT_ASSERT ( buffer == ParseInfo ( buffer ).next() );
}

void 
ParseInfoTest::testPrevious()
{
  fillBuffer ( buffer, SEQUENCE_HEADER, 100, 400 );
  CPPUNIT_ASSERT ( buffer - 400 == ParseInfo ( buffer ).previous() );
  fillBuffer ( buffer, SEQUENCE_HEADER, 100, 0 );
  CPPUNIT_ASSERT ( buffer == ParseInfo ( buffer ).previous() );               
}

void 
ParseInfoTest::testSize()
{
  fillBuffer ( buffer, SEQUENCE_HEADER, 100, 0 );
  CPPUNIT_ASSERT ( static_cast<uint32> ( 100 )
                 == ParseInfo ( buffer ).size() );
  CPPUNIT_ASSERT ( static_cast<uint32> ( 200 ) != ParseInfo ( buffer ).size() );
                    
  fillBuffer ( buffer, SEQUENCE_HEADER, 400, 0 );
  CPPUNIT_ASSERT  ( static_cast<uint32> ( 400 ) == ParseInfo ( buffer ).size() );
                    
  fillBuffer ( buffer, SEQUENCE_HEADER, 60000, 0 );
  CPPUNIT_ASSERT ( static_cast<uint32> ( 60000 ) == ParseInfo ( buffer ).size() );
  
  fillBuffer ( buffer, INTRA_NON_REF_LD, 300000, 0 );
  CPPUNIT_ASSERT ( static_cast<uint32> ( 300000 ) == ParseInfo ( buffer ).size() ); 
                    
  fillBuffer ( buffer, END_OF_SEQUENCE, 0, 0 );
  CPPUNIT_ASSERT ( static_cast<uint32> ( 13 ) == ParseInfo ( buffer ).size() );
}

void 
ParseInfoTest::fillBuffer ( uint8* buffer, uint8 parseCode
                          , uint32 next, uint32 prev)
{
  *buffer++ = PIH_1;
  *buffer++ = PIH_2;
  *buffer++ = PIH_3;
  *buffer++ = PIH_4;
  *buffer++ = parseCode;
  write4Bytes ( buffer, next );
  write4Bytes ( buffer + 4, prev );
}

void 
ParseInfoTest::write4Bytes ( uint8* buffer, uint32 data )
{
  *buffer++ = static_cast<uint8> ( ( data & 0xFF000000 ) >> 24 );
  *buffer++ = static_cast<uint8> ( ( data & 0x00FF0000 ) >> 16 );
  *buffer++ = static_cast<uint8> ( ( data & 0x0000FF00 ) >> 8 );
  *buffer   = static_cast<uint8> ( data & 0x000000FF );
}
