/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: StreamReader.hpp 67 2008-03-04 13:16:25Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _STREAMREADER_HPP_
#define _STREAMREADER_HPP_

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <common/dirac.h>
#include <common/util.hpp>

#include <parser/IInput.hpp>

#include <vector>
#include <memory>
#include <stdexcept>
#include <cstring>


namespace dirac
{
namespace parser_project
{
  //! An exception class for StreamReader
  class StreamReaderException : public std::runtime_error 
  {
  public:
    StreamReaderException ( std::string const& message )
    : std::runtime_error ( message ) {}
  };

  //! A stream buffer class loosely based on streambuf to manage buffered reads 
  //! from any medium
  class StreamReader
  {
    friend class ValidityChecker<StreamReader>;
    
  public: 
    typedef StreamReader                              class_type;
    typedef std::vector<uint8>                        Buffer;
    typedef Buffer::value_type                        value_type;
    typedef Buffer::pointer                           pointer;
    typedef Buffer::const_pointer                     const_pointer;
    typedef Buffer::size_type                         size_type;
  
  public: 
    explicit StreamReader  ( std::auto_ptr<IInput> input
                  , size_type putback_page_number = PUT_BACK_PAGE_N )
    : input_ ( input ), buffer_ ( PAGE_SIZE * putback_page_number * BUFFER_SIZE )
    , eback_ ( 0 ), gptr_ ( 0 ), egptr_ ( 0 ), global_offset_ ( 0 )
    {
      DIRAC_MESSAGE_ASSERT  ( "buffer cannot be empty", !buffer_.empty() );
      DIRAC_MESSAGE_ASSERT  ( "input cannot be null", 0 != input_.get() );
      
      eback_ = &buffer_[0] + PAGE_SIZE * putback_page_number;
      gptr_ = egptr_ = eback_;
      if ( !addDataFwd() ) 
        throw StreamReaderException ( "no data from input" );
      checkValidity(); 
    }
    ~StreamReader() { checkValidity(); }
  private: // compiler generated methods are not allowed
    StreamReader ( class_type const& );
    class_type& operator= ( class_type const& );
    
  public:
    bool                addDataFwd()
    {
      ValidityChecker<class_type> checker ( *this );
      size_type original_size = buffer_.size() - putback_size();
      size_type target = original_size - sizeFwd();
      if ( sizeFwd() < sizeBwd() ) {
        // this is a safe cast
        memcpy ( const_cast<pointer> ( eback_ ), gptr_, sizeFwd() );
      }
      else {
        // this is a safe cast
        memmove (const_cast<pointer> ( eback_ ), gptr_, sizeFwd() );
      }
      size_type bytesread = 0;
      while ( input_->isAvailable() && bytesread < target ) {
        bytesread += 
          input_->read ( const_cast<pointer> ( eback_ ) + sizeFwd() + bytesread
                       , target - bytesread );
      }
      setg  ( eback_, eback_, eback_ + sizeFwd() + bytesread );
      global_offset_ += bytesread;
      return bytesread;
    }
    
    bool                addDataBwd()
    {
      if ( !input_->backwardScanSupported() ) return false;
      if ( sizeBwd() >static_cast<size_t> ( eback_ - &buffer_[0] ) )
        throw StreamReaderException ( "Putback is too small, increase buffer size" );
      memcpy ( &buffer_[0], eback_, sizeBwd() );
      size_type target = buffer_.size() - sizeBwd() - putback_size();
      if ( !input_->rewind ( target ) ) return false;
      size_type bytesread = 0;
      while ( input_->isAvailable() && bytesread < target ) {
        // this cast is safe
        bytesread += input_->read ( const_cast<pointer> ( eback_ ) + bytesread, target - bytesread );
      }
      // this cast is safe
      memcpy ( const_cast<pointer> ( eback_ ) + bytesread, &buffer_[0], sizeBwd() );
      setg  ( eback_
            , eback_ + bytesread + sizeBwd()
            , eback_ + bytesread + sizeBwd() );
      global_offset_ -= bytesread;
      return bytesread;
    }
    
    const_pointer     eback() const  { checkValidity(); return eback_; }
    const_pointer&    gptr()         { checkValidity(); return gptr_; }
    const_pointer     egptr() const  { checkValidity(); return egptr_; }
    size_type         size() const   { checkValidity(); return egptr_ - eback_; }
    size_type         sizeFwd() const { checkValidity(); return egptr_ - gptr_; }
    size_type         sizeBwd() const { checkValidity(); return gptr_ - eback_; }

    //! rewind the associated physical medium to the beginning of the stream
    //! if possible
    bool              rewind()
    {
      ValidityChecker<class_type> checker ( *this );
      bool ret = input_->rewind();
      if ( ret ) {
        setg ( eback(), eback(), eback() );
        global_offset_ = 0;
        return addDataFwd();
      }
      else return false;
    }
    
    void              swap ( class_type& rhs )
    {
      ValidityChecker<class_type> checker ( *this );
      std::auto_ptr<IInput> tmp ( input_.release() );
      input_ = rhs.input_;
      rhs.input_ = tmp;
      buffer_.swap ( rhs.buffer_ );
      std::swap ( eback_, rhs.eback_ );
      std::swap ( gptr_, rhs.gptr_ );
      std::swap ( egptr_, rhs.egptr_ );
      std::swap ( global_offset_, rhs.global_offset_ );
    }
    
    int64             goffset() const
    { checkValidity(); return global_offset_ - sizeFwd(); }
    
    bool              isAvailable() const
    { checkValidity(); return input_->isAvailable(); }

  private: 
    void              checkValidity() const
    {
      DIRAC_MESSAGE_ASSERT  ( "internal pointer cannot be null"
                            , 0 != eback_ && 0 != gptr_ && 0 != egptr_ );
      DIRAC_MESSAGE_ASSERT  ( "eback must lie within buffer"
                            , !( eback_ < &buffer_[0] )
                            && !( eback_ > &buffer_[0] + buffer_.size() ) );
      DIRAC_MESSAGE_ASSERT  ( "egptr must lie within buffer"
                            , !( egptr_ < &buffer_[0] )
                            && !( egptr_ > &buffer_[0] + buffer_.size() ) );
      DIRAC_MESSAGE_ASSERT  ( "gptr must lie within eback and egptr"
                            , !( gptr_ < eback_) && !( gptr_ > egptr_ ) );
    }

    void              setg  ( const_pointer eback, const_pointer gptr
                            , const_pointer egptr )
    {
      eback_  = eback;
      gptr_   = gptr;
      egptr_  = egptr;
    }
    
    size_type         putback_size() const
    {
      return static_cast<size_type> ( eback_ - &buffer_[0] );
    }
    
  private:
    enum {
        PAGE_SIZE       = 4096
      , PUT_BACK_PAGE_N = 500
      , BUFFER_SIZE     = 10
    };

    std::auto_ptr<IInput>       input_;
    Buffer                      buffer_;
    const_pointer               eback_, gptr_, egptr_;
    int64                       global_offset_;
  };
    
} /* parse_project */

} /* dirac */


#endif /* _STREAMREADER_HPP_ */
