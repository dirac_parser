/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: AccessUnit.hpp 67 2008-03-04 13:16:25Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _ACCESSUNIT_HPP_
#define _ACCESSUNIT_HPP_

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <common/dirac.h>
#include <common/util.hpp>

#include <parser/parser.h>

#include <vector>
#include <stdexcept>
#include <algorithm>

namespace dirac
{
namespace parser_project
{
  struct AccessUnitException : public std::runtime_error 
  {
    AccessUnitException ( std::string const& msg ) 
    : std::runtime_error ( msg ) {}
  };
  
  class AccessUnit
  {
  friend class ValidityChecker<AccessUnit>;
  
  public: // type definitions
    typedef AccessUnit                                  class_type;
    typedef std::vector<uint8>                          Buffer;
    typedef Buffer::value_type                          value_type;
    typedef Buffer::pointer                             pointer;
    typedef Buffer::const_pointer                       const_pointer;
    typedef Buffer::size_type                           size_type;
    
  public: // constructors
    AccessUnit() : buffer_() {}
    AccessUnit ( const_pointer begin, size_type size ) 
    : buffer_() 
    {
      setp ( begin, size );
    }
    ~AccessUnit() { checkValidity(); }
  private: // compiler generated methods are not allowed
    AccessUnit ( AccessUnit const& );
    AccessUnit& operator= ( AccessUnit const& );
    
  public: // main interface
    void                          setp ( const_pointer begin, size_type size )
    {
      ValidityChecker<class_type>  checker ( *this );

      if ( !dirac_parser_is_access_unit ( begin, size ) )
        throw AccessUnitException ( "this is not an access unit" );
      Buffer tmp ( begin, begin + size );
      buffer_.swap ( tmp );
    }
    
    //! release internal buffer - it doesn't check whether the buffer is empty
    Buffer*                       releaseBuffer()
    {
      ValidityChecker<class_type>  checker ( *this );

      Buffer* buf = new Buffer;
      std::swap ( buffer_, *buf );
      return buf;
    }
    
    const_pointer                 begin() const
    { checkValidity(); return empty() ? 0 : &( *buffer_.begin() ); }
    pointer                       begin() 
    { checkValidity(); return empty() ? 0 : &( *buffer_.begin() ); }
    const_pointer                 end() const
    { checkValidity(); return empty() ? 0 : &(*buffer_.begin() ) + size(); }
    size_type                     size() const
    { checkValidity(); return buffer_.size(); }
    bool                          empty() const
    { checkValidity(); return buffer_.empty(); }
    
    bool                          isSequenceHeader() const
    { 
      checkValidity(); 
      return dirac_parser_is_sequence_header ( begin(), size() );
    }
    bool                          isPicture() const
    {
      checkValidity();
      return dirac_parser_is_picture ( begin(), size() );
    }
    bool                          isIPicture() const
    {
      checkValidity();
      return dirac_parser_is_i_picture ( begin(), size() );
    }
    bool                          isAuxiliaryData() const
    {
      checkValidity();
      return dirac_parser_is_auxiliary_data ( begin(), size() );
    }
    bool                          isPaddingData() const
    {
      checkValidity();
      return dirac_parser_is_padding_data ( begin(), size() );
    }
    bool                          isEndOfSequence() const
    {
      checkValidity();
      return dirac_parser_is_end_of_sequence ( begin(), size() );
    }
    bool                          isLastEndOfSequence() const
    {
      checkValidity();
      return dirac_parser_is_last_end_of_sequence ( begin(), size() );
    }
        
  private: // helper methods
    void                          checkValidity() const
    {
      DIRAC_MESSAGE_ASSERT  ( "buffer empty or must hold access unit"
      ,  buffer_.empty() 
      || dirac_parser_is_access_unit ( &buffer_[0], buffer_.size() ) );
    }
  private: // members
    Buffer                                      buffer_;
  };
} /* parser_project */

} /* dirac */


#endif /* _ACCESSUNIT_HPP_ */
