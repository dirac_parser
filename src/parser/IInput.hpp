/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: IInput.hpp 58 2008-02-17 17:16:20Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _IINPUT_HPP_
#define _IINPUT_HPP_

#include <common/dirac.h>

#include <cstdlib>

namespace dirac
{
namespace parser_project
{
  class IInput
  {
  public: // type definitions
    typedef IInput              class_type; // self
    typedef uint8               value_type;
    typedef value_type*         pointer;
    typedef value_type const*   const_pointer;
    typedef size_t              size_type;
    
  public: // constructors
    IInput ( bool bwdScan ) : bwdScan_ ( bwdScan ) {}
    virtual ~IInput() {}
  private:  // compiler generated methods are not allowed
    IInput ( IInput const& );
    IInput& operator= ( IInput const& );
    
  public: // main methods
    //! true if the input stream has no errors and has not reached the end
    bool                        isAvailable() const
    { return available(); }
    size_type                   read ( pointer begin, size_type count)
    { 
      DIRAC_MESSAGE_ASSERT ( "pointer cannot be null", 0 != begin );
      return readInput ( begin, count ); 
    }
    bool                        backwardScanSupported() const
    { return bwdScan_; }
    bool                        rewind ( size_type count )
    { return rewindStream ( count ); }
    bool                        rewind()
    { return fullRewind(); }
    
  private:  // virtual interface
    virtual bool                available() const                   =0;
    virtual size_type           readInput ( pointer         begin
                                          , size_type       count ) =0;
    virtual bool                rewindStream ( size_type    count ) =0;
    virtual bool                fullRewind() =0;
    
  protected:
    bool                        bwdScan_;
  };
} /* parser_project */

} /* dirac */


#endif /* _IINPUT_HPP_ */
