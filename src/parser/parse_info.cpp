/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: parse_info.cpp 67 2008-03-04 13:16:25Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <parser/parse_info.hpp>

#include <common/util.hpp>

namespace dirac
{
namespace parser_project
{
  ParseInfo::ParseInfoPrefix::value_type const
    ParseInfo::ParseInfoPrefix::prefix_[] = { 0x42, 0x42, 0x43, 0x44 };
  ParseInfo::ParseInfoPrefix ParseInfo::prefix_;
  
  enum {
      PI_HEADER_LENGTH = 13
    , PI_PREFIX_LENGTH = 4
    
    // parse code
    , CHECK_SEQUENCE_HEADER     = 0xFF
    , SEQUENCE_HEADER_VALUE     = 0x00

    , CHECK_END_OF_SEQUENCE     = 0xFF
    , END_OF_SEQUENCE_VALUE     = 0x10

    , CHECK_AUXILIARY_DATA      = 0xF8
    , AUXILIARY_DATA_VALUE      = 0x20

    , CHECK_PADDING_DATA        = 0xFF // I think, rather than F8, is appr. here
    , PADDING_DATA_VALUE        = 0x30
    
    , CHECK_PICTURE             = 0x08
    , PICTURE_VALUE             = 0x08

    , CHECK_CORE_SYNTAX         = 0x88
    , CORE_SYNTAX_VALUE         = 0x08

    , CHECK_LOW_DELAY_SYNTAX    = 0x88
    , LOW_DELAY_SYNTAX_VALUE    = 0x88

    , CHECK_USING_AC            = 0x48
    , USING_AC_VALUE            = 0x08
    
    , CHECK_IS_REFERENCE        = 0x0C
    , IS_REFERENCE_VALUE        = 0x0C
    
    , CHECK_IS_NON_REFERENCE    = 0x0C
    , IS_NON_REFERENCE_VALUE    = 0x08
    
    , NUMBER_OF_REFERENCES      = 0x03

    // offsets
    , PC_OFFSET                 = 4     // parse code offset
    , NPO_OFFSET                = 5     // next parse offset's offset
    , PPO_OFFSET                = 9     // previous parse offset's offset
    , RPPO_OFFSET               = 5     // previous parse offset's offset
                                        // from the reverse cursor's base
    // other constants
    , DATA_OK                   = 1
    , NO_DATA                   = 0
    
  };
  
} /* parser_project */

} /* dirac */


namespace parser = ::dirac::parser_project;
using namespace parser;

/*  ParseInfo::ParseInfoPrefix    */

ParseInfo::ParseInfoPrefix::const_iterator
ParseInfo::ParseInfoPrefix::begin()
{
  return prefix_;
}

ParseInfo::ParseInfoPrefix::const_iterator
ParseInfo::ParseInfoPrefix::end()
{
  return prefix_ + DIRAC_ARRAY_SIZE ( prefix_ );
}

ParseInfo::ParseInfoPrefix::size_type
ParseInfo::ParseInfoPrefix::size()
{
  return PI_PREFIX_LENGTH;
}

ParseInfo::ParseInfoPrefix::const_reverse_iterator
ParseInfo::ParseInfoPrefix::rbegin() 
{
  return std::reverse_iterator<const_iterator> ( end() );
}

ParseInfo::ParseInfoPrefix::const_reverse_iterator
ParseInfo::ParseInfoPrefix::rend() 
{
  return std::reverse_iterator<const_iterator> ( begin() );
}

bool 
ParseInfo::ParseInfoPrefix::isParseInfo ( const_iterator cursor, size_type size )
{
  DIRAC_MESSAGE_ASSERT ( "cursor cannot be null", 0 != cursor );
  return (  !( PI_HEADER_LENGTH > size )
        &&  prefix_[0] == cursor[0] && prefix_[1] == cursor[1] 
        &&  prefix_[2] == cursor[2] && prefix_[3] == cursor[3] );
}

ParseInfo::const_iterator
ParseInfo::prefixBegin()
{
  return prefix_.begin();
}

ParseInfo::const_iterator
ParseInfo::prefixEnd()
{
  return prefix_.end();
}

ParseInfo::const_reverse_iterator
ParseInfo::prefixRbegin()
{
  return prefix_.rbegin();
}

ParseInfo::const_reverse_iterator
ParseInfo::prefixRend()
{
  return prefix_.rend();
}

ParseInfo::size_type
ParseInfo::prefixSize()
{
  return prefix_.size();
}


/*  ParseInfo */

ParseInfo::ParseInfo ( const_iterator cursor )
  : cursor_ ( cursor ) 
{
  checkValidity();
}

ParseInfo::~ParseInfo()
{
  checkValidity();
}

void
ParseInfo::checkValidity() const
{
  DIRAC_MESSAGE_ASSERT  ( "parse info header cannot be null", 0 != cursor_ );
}

ParseInfo::value_type
ParseInfo::getParseCode() const
{
  return cursor_[PC_OFFSET];
}

bool
ParseInfo::isParseInfo ( const_iterator buffer, size_type size )
{
  DIRAC_MESSAGE_ASSERT ( "buffer cannot be null", 0 != buffer );
  return prefix_.isParseInfo ( buffer, size );
}

bool
ParseInfo::isSequenceHeader() const
{
  checkValidity();
  return SEQUENCE_HEADER_VALUE == ( CHECK_SEQUENCE_HEADER & getParseCode() );
}

bool
ParseInfo::isEndOfSequence() const
{
  checkValidity();
  return END_OF_SEQUENCE_VALUE == ( CHECK_END_OF_SEQUENCE & getParseCode() );
}

bool
ParseInfo::isAuxiliaryData() const
{
  checkValidity();
  return AUXILIARY_DATA_VALUE == ( CHECK_AUXILIARY_DATA & getParseCode() );
}

bool
ParseInfo::isPaddingData() const
{
  checkValidity();
  return PADDING_DATA_VALUE == ( CHECK_PADDING_DATA & getParseCode() );
}

bool
ParseInfo::isPicture() const
{
  checkValidity();
  return PICTURE_VALUE == ( CHECK_PICTURE & getParseCode() );
}

bool
ParseInfo::isCoreSyntax() const
{
  checkValidity();
  return CORE_SYNTAX_VALUE == ( CHECK_CORE_SYNTAX & getParseCode()  );
}

bool
ParseInfo::isLowDelaySyntax() const
{
  checkValidity();
  return LOW_DELAY_SYNTAX_VALUE == ( CHECK_LOW_DELAY_SYNTAX & getParseCode() );
}

bool
ParseInfo::isUsingArithmeticCoding() const
{
  checkValidity();
  return USING_AC_VALUE == ( CHECK_USING_AC & getParseCode() );
}

bool
ParseInfo::isReference() const
{
  checkValidity();
  return IS_REFERENCE_VALUE == ( CHECK_IS_REFERENCE & getParseCode() );
}

bool
ParseInfo::isNotReference() const
{
  checkValidity();
  return IS_NON_REFERENCE_VALUE == ( CHECK_IS_NON_REFERENCE & getParseCode() );
}

ParseInfo::value_type
ParseInfo::getNumberOfReferences() const
{
  checkValidity();
  return NUMBER_OF_REFERENCES & getParseCode();
}

bool
ParseInfo::isIntra() const
{
  checkValidity();
  return isPicture() && 0 == getNumberOfReferences();
}

bool
ParseInfo::isInter() const
{
  checkValidity();
  return isPicture() && getNumberOfReferences();
}

bool
ParseInfo::hasPayload() const
{
  checkValidity();
  return END_OF_SEQUENCE_VALUE != getParseCode();
}

ParseInfo::const_iterator
ParseInfo::payload() const
{
  checkValidity();
  return cursor_ + PI_HEADER_LENGTH;
}

ParseInfo::const_iterator
ParseInfo::next() const
{
  checkValidity();
  
  return cursor_ + readFourBytes ( cursor_ + NPO_OFFSET );
}

ParseInfo::const_iterator
ParseInfo::previous() const
{
  checkValidity();
  
  return cursor_ - readFourBytes ( cursor_ + PPO_OFFSET );
}

ParseInfo::size_type
ParseInfo::size() const
{
  checkValidity();
  size_type next = readFourBytes ( cursor_ + NPO_OFFSET );
  return 0 == next ? PI_HEADER_LENGTH : next;
}
