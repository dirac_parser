/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: parse_info.hpp 48 2008-02-12 22:48:00Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _PARSE_INFO_HPP_
#define _PARSE_INFO_HPP_

#include <common/dirac.h>
#include <parser/parser.h>

#include <memory>

namespace dirac
{
namespace parser_project
{
  /*  This class is used to identify and manage dirac access units. It can
      recognise whether a buffer contains a parse info header and it can decode
      the individual components carried in the PIH.
      Note that an object of type ParseInfo can only be created if we know
      the start of PIH and that the passed buffer contains the entire PIH.
      The class provides a complement of static methods to determine whether
      a PIH is fully contained in a passed buffer. If an object of this
      class is instantiated on a buffer not caontaining at least 13 bytes or
      not starting with the PIH the resulting behaviour is undefined.  */
  class ParseInfo
  {
  public:
    typedef uint8                                       value_type;
    typedef uint32                                      size_type;
    typedef value_type const*                           const_iterator;
    typedef value_type const*                           const_pointer;
    typedef value_type                                  const_reference;
    typedef std::reverse_iterator<const_iterator>       const_reverse_iterator;
    
  public: // construction
    explicit ParseInfo ( const_iterator cursor );
    ~ParseInfo();
    // compiler generated methods are OK
    
  private: // helper class
    class ParseInfoPrefix
    {
    public: // types
      typedef uint8                                       value_type;
      typedef value_type const*                           const_iterator;
      typedef std::reverse_iterator<const_iterator>       const_reverse_iterator;
      typedef uint32                                      size_type;

    public: // construction
      // compiler generated methods are OK
      
    public: // main methods
      static const_iterator         begin();
      static const_iterator         end();
      static const_reverse_iterator rbegin();
      static const_reverse_iterator rend();
      static size_type              size();
      
      static bool isParseInfo ( const_iterator cursor, size_type size );
      
    private: // members
      static const value_type prefix_[];
    };
    
  public: // main methods
    bool              isSequenceHeader()                          const;
    bool              isEndOfSequence()                           const;
    bool              isAuxiliaryData()                           const;
    bool              isPaddingData()                             const;
    bool              isPicture()                                 const;
    bool              isCoreSyntax()                              const;
    bool              isLowDelaySyntax()                          const;
    bool              isUsingArithmeticCoding()                   const;
                                                                            
    bool              isReference()                               const;
    bool              isNotReference()                            const;
    bool              isIntra()                                   const;
    bool              isInter()                                   const;
    value_type        getNumberOfReferences()                     const;
    
    // data unit
    // false if end of sequence, true otherwise                                                                          
    bool              hasPayload()                                const;
    // returns start of payload - it assumes access unit has a payload,
    // undefined behaviour if it doesn't
    const_iterator    payload()                                   const;
    //! returns the pointer obtained by adding the next field to cursor
    const_iterator    next()                                      const;
    //! returns the pointer obtained by subtracting the previous field to cursor
    const_iterator    previous()                                  const;
    // the size of the access unit is deducted from the next field in the PIH
    // it is assumed that only a END OF SEQUENCE access unit can have a next
    // field set to 0 (in which case the size is 13).
    size_type         size()                                      const;
    
    /*  checks that the cursor points to the start of a parse info header
        and that there is enough room in the buffer to contain the entire
        PIH   */
    static bool       isParseInfo ( const_iterator cursor, size_type size );
    
    static const_iterator               prefixBegin();
    static const_iterator               prefixEnd();
    static const_reverse_iterator       prefixRbegin();
    static const_reverse_iterator       prefixRend();
    static size_type                    prefixSize();
    
  
  public: // helper member
    
  private: // utils
    void              checkValidity()                             const;
    value_type        getParseCode()                              const;
    
  private:  // members
    /*  reference to start of parse info header */
    const_iterator    cursor_;
    static            ParseInfoPrefix   prefix_;
  };
  
  
  
} /* parser_project */

} /* dirac */


#endif /* _PARSE_INFO_HPP_ */
