/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: scanner.cpp 67 2008-03-04 13:16:25Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <parser/scanner.hpp>
#include <parser/parse_info.hpp>

#include <algorithm>

namespace parser = ::dirac::parser_project;
using namespace parser;


Scanner::Scanner  ( const_iterator& cursor, size_type sizeFwd )
  : buffer_ ( cursor, sizeFwd ) {}
  
Scanner::Scanner  ( const_iterator& cursor
                  , size_type sizeFwd
                  , size_type sizeBwd )
  : buffer_  ( cursor, sizeFwd, sizeBwd )  {}

Scanner::~Scanner() {}



/*  scan the associated buffer forward for the next access unit
    it starts by looking for a parse info header and then it ensures
    that the entire access unit associated with that PIH is contained in
    the buffer by looking for the next PIH.
    There is one exception, the final end of sequence PIH. In that case the
    PIH doesn't have any payload and may be the last element in the stream;
    if a EoS is detected and there is no further access unit then the function
    returns true.
    Returns true if it finds an access unit; the cursor of the associated
    buffer will point to the start of the access unit.
    Returns false if it cannot find a PIH or the access unit is not 
    completely contained in the buffer associated with buffer_. In this case
    the cursor of the associated buffer is moved as far forward as
    required (ie, the end of the buffer - 3, if we couldn't find anything,
    or the beginning of the PIH if the associated access unit didn't fit
    in the buffer)   */
bool
Scanner::scanForward()
{
  if ( isBufferInSyncFwd() ) return true;
  // ok, the bitstream is not in sync, start scanning the buffer forward
  do {
    const_iterator cursor = 
              std::search ( static_cast<const_iterator> ( buffer_.cursor() )
                          , buffer_.end()
                          , ParseInfo::prefixBegin()
                          , ParseInfo::prefixEnd() );
    if ( buffer_.end() == cursor ) {
      // I need more data, couldn't find PIH - rewind the cursor a little bit
      buffer_.cursor() = cursor - ParseInfo::prefixSize() + 1;
      return false;
    }
    else {
      // check that there is enough data for the entire parse info header
      if ( !ParseInfo::isParseInfo ( cursor, buffer_.end() - cursor ) ) {
        buffer_.cursor() = cursor;
        return false;
      }
      else {
        // ok, enough data for PIH, now check that the next PI is in the buffer
        ParseInfo firstPi ( cursor );
        const_iterator nextAccessUnit = firstPi.next();
        if ( nextAccessUnit > buffer_.end() ) {
          if ( nextAccessUnit - cursor > MAX_AU_SIZE ) {
            // probably some corruption...
            ++buffer_.cursor();
            continue;
          }
          else {
            // just ask for more data
            buffer_.cursor() = cursor;
            return false;
          }
        }
        else {
          // now check that we have a genuine PIH if enough data in buffer
          if ( nextAccessUnit + ParseInfo::prefixSize() < buffer_.end() ) {
            const_iterator nextPi = 
                          std::search ( nextAccessUnit
                                      , nextAccessUnit + ParseInfo::prefixSize()
                                      , ParseInfo::prefixBegin()
                                      , ParseInfo::prefixEnd() );
            if ( nextAccessUnit + ParseInfo::prefixSize() == nextPi ) {
              ++buffer_.cursor();
              continue;
            }
            else {
              buffer_.cursor() = cursor;
              return true;
            }
          }
          else {
            buffer_.cursor() = cursor;
            return true;
          }
        }
      }
    }
  } while ( true );
}
      
bool
Scanner::isBufferInSyncFwd()
{
  int bufferLength = buffer_.end() - buffer_.cursor();
  if ( ParseInfo::isParseInfo ( buffer_.cursor(), bufferLength ) ) {
    const_iterator next = ParseInfo ( buffer_.cursor() ).next();
    if ( next > buffer_.end() )
      return false;
    if ( buffer_.cursor() == next) {
      return ParseInfo ( buffer_.cursor() ).isEndOfSequence();
    }
    return ParseInfo::isParseInfo ( next, buffer_.end() - next );
  }
  return false;
}
  
bool
Scanner::scanBackward()
{
  if ( isBufferInSyncBwd() ) return true;
  // ok the bitstream is not in sync, start scanning backward
  do {
    const_reverse_iterator rcursor  
      ( std::search ( const_reverse_iterator ( buffer_.cursor() )
                    , buffer_.rend()
                    , ParseInfo::prefixRbegin()
                    , ParseInfo::prefixRend() ) );
    if ( buffer_.rend() == rcursor ) {
      // I need more data, I couldn't find a PI prefix
      buffer_.cursor() = rcursor.base() + ParseInfo::prefixSize() - 1;
      return false;
    }
    else {
      // found first PI, now look for previous one
      // note that rcursor doesn't point to start of PI
      if  ( !ParseInfo::isParseInfo ( rcursor.base() - ParseInfo::prefixSize()
          , buffer_.end() - rcursor.base() + ParseInfo::prefixSize() ) ) {
        buffer_.cursor() = rcursor.base() - 1;
        continue;        
      }
      ParseInfo firstPi ( rcursor.base() - ParseInfo::prefixSize() );
      const_iterator prevPi = firstPi.previous();
      if ( prevPi < buffer_.begin() ) {
        // I may need more data
        if ( buffer_.cursor() - prevPi > MAX_AU_SIZE ) {
          buffer_.cursor() = rcursor.base() - 1;
          continue;
        }
        else {
          buffer_.cursor() = rcursor.base();
          return false;
        }
      }
      else {
        if  (  prevPi == buffer_.cursor() 
            && !ParseInfo ( prevPi ).isSequenceHeader() )  {
          buffer_.cursor() = prevPi - 1;
          continue;
        }
        if ( !ParseInfo::isParseInfo ( prevPi, buffer_.end() - prevPi ) ) {
          buffer_.cursor() = rcursor.base() - 1;
          continue;
        }
        else {
          buffer_.cursor() = prevPi;
          return true;
        }
      }
    }
  } while ( true );
}

bool
Scanner::isBufferInSyncBwd()
{
  int bufferLength = buffer_.end() - buffer_.cursor();
  if ( ParseInfo::isParseInfo ( buffer_.cursor(), bufferLength ) ) {
    const_iterator previous = 
          ParseInfo ( buffer_.cursor() ).previous();
    if ( previous < buffer_.begin() ) return false;
    if ( buffer_.cursor() == previous ) {
      return false;  // we found the first access unit in the stream
    }
    buffer_.cursor() = previous;
    return true;
  }
  return false;
}
