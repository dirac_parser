/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: parser.cpp 67 2008-03-04 13:16:25Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <parser/parser.h>

#include <parser/version.h>

#include <parser/scanner.hpp>
#include <parser/parse_info.hpp>

#include <common/util.hpp>

namespace  
{
  enum {
      NO_DATA = 0
    , DATA_OK
  };
  
  using parser::ParseInfo;
  
  /*  returns true if it is possible to move the cursor to the start of the
      next access unit. The cursor must point to the start of an access unit
      or else undefined behaviour will follow.
      Returns false upon failure   */
  bool
  moveCursorForward ( uint8 const*& cursor, size_t length )
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor must point to start of access unit"
                  , ParseInfo::isParseInfo ( cursor, length ) );

    uint8 const* next = ParseInfo ( cursor ).next();
    if ( next > cursor && next < (cursor + length) ) {
      cursor = next;
      return true;
    }
    else
      return false;
  }
  
} /*   */

namespace dirac
{
namespace parser_project
{
  
  int
  dirac_parser_get_sequence_header  ( uint8 const** cursor_reference
                                    , size_t        buffer_size_fwd
                                    , size_t*       seq_hdr_size_reference )
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor_reference cannot be null"
                          , 0 != cursor_reference );
    DIRAC_MESSAGE_ASSERT  ( "seq_hdr_size_reference cannot be null"
                          , 0 != seq_hdr_size_reference );

    Scanner scanner ( *cursor_reference, buffer_size_fwd );
    while ( scanner.scanForward() ) {
      ParseInfo pi ( scanner.cursor() );
      if ( pi.isSequenceHeader() ) {
        *seq_hdr_size_reference = pi.size();
        return DATA_OK;
      }
      else if ( !moveCursorForward ( *cursor_reference, scanner.sizeFwd() ) ) {
        return NO_DATA;
      }
    }
    return NO_DATA;
  }
  
  int
  dirac_parser_get_next_picture ( uint8 const** cursor_reference
                                , size_t        buffer_size_fwd
                                , size_t*       pic_size_reference)
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor_reference cannot be null"
                          , 0 != cursor_reference );
    DIRAC_MESSAGE_ASSERT  ( "pic_size_reference cannot be null"
                          , 0 != pic_size_reference );

    Scanner scanner ( *cursor_reference, buffer_size_fwd );
    while ( scanner.scanForward() ) {
      ParseInfo pi ( scanner.cursor() );
      if ( pi.isPicture() ) {
        *pic_size_reference = pi.size();
        return DATA_OK;
      }
      else if ( !moveCursorForward ( *cursor_reference, scanner.sizeFwd() ) ) {
        return NO_DATA;
      }
    }
    return NO_DATA;
  }
  
  int
  dirac_parser_get_next_access_unit ( uint8 const** cursor_reference
                                    , size_t        buffer_size_fwd
                                    , size_t*       acc_unit_size_reference )
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor_reference cannot be null"
                          , 0 != cursor_reference );
    DIRAC_MESSAGE_ASSERT  ( "acc_unit_size_reference cannot be null"
                          , 0 != acc_unit_size_reference );
                          
    Scanner scanner ( *cursor_reference, buffer_size_fwd );
    if ( scanner.scanForward() ) {
      *acc_unit_size_reference = ParseInfo ( scanner.cursor() ).size();
      return DATA_OK;
    }
    else {
      return NO_DATA;
    }
  }
  
  int
  dirac_parser_get_next_i_picture ( uint8 const** cursor_reference
                                  , size_t        buffer_size_fwd
                                  , size_t*       pic_size_reference )
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor_reference cannot be null"
                          , 0 != cursor_reference );
    DIRAC_MESSAGE_ASSERT  ( "pic_size_reference cannot be null"
                          , 0 != pic_size_reference );

    Scanner scanner ( *cursor_reference, buffer_size_fwd );
    while ( scanner.scanForward() ) {
      ParseInfo pi ( scanner.cursor() );
      if ( pi.isIntra() ) {
        *pic_size_reference = pi.size();
        return DATA_OK;
      }
      else if ( !moveCursorForward ( *cursor_reference, scanner.sizeFwd() ) ) {
        return NO_DATA;
      }
    }
    return NO_DATA;
  }
  
  int
  dirac_parser_seek_and_get_pic     ( uint8 const** cursor_reference
                                    , size_t        buffer_size_fwd
                                    , size_t        buffer_size_bwd
                                    , int*          n
                                    , size_t*       pic_size_reference
                                    , bool          isIntra )
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor_reference cannot be null"
                          , 0 != cursor_reference );
    DIRAC_MESSAGE_ASSERT  ( "pic_size_reference cannot be null"
                          , 0 != pic_size_reference );
    DIRAC_MESSAGE_ASSERT  ( "n cannot be null", 0 != n );
    
    Scanner scanner ( *cursor_reference, buffer_size_fwd, buffer_size_bwd );
    if ( 0 > *n ) {  // scan backward
      while ( scanner.scanBackward() ) {
        ParseInfo pi ( scanner.cursor() );
        if ( isIntra ) {
          if ( pi.isIntra() && 0 == ++(*n) ) {
            *pic_size_reference = pi.size();
            return DATA_OK;
          }
        }
        else {
          if ( pi.isPicture() && 0 == ++(*n) ) {
            *pic_size_reference = pi.size();
            return DATA_OK;
          }
        }
        // note: we don't need to move the cursor for backward scanning
      }
      return NO_DATA;
    }
    else {  // scan forward
      while ( scanner.scanForward() ) {
        ParseInfo pi ( scanner.cursor() );
        if ( isIntra ) {
          if ( pi.isIntra() && 0 == (*n)-- ) {
            *pic_size_reference = pi.size();
            ++(*n); // we need this because we want to use 0 to request the next
            // i pic forward - lack of symmetry with backward search - FIXME
            return DATA_OK;
          }
        }
        else {
          if ( pi.isPicture() && 0 == (*n)-- ) {
            *pic_size_reference = pi.size();
            ++(*n); // we need this because we want to use 0 to request the next
            // i pic forward - lack of symmetry with backward search - FIXME
            return DATA_OK;
          }
        }
        if ( !moveCursorForward ( *cursor_reference, scanner.sizeFwd() ) ) {
          return NO_DATA;
        }
      }
      return NO_DATA;
    }
  }
  
  int
  dirac_parser_get_picture_skip_n   ( uint8 const** cursor_reference
                                    , size_t        buffer_size_fwd
                                    , size_t        buffer_size_bwd
                                    , int*          n
                                    , size_t*       pic_size_reference )
  {
    return dirac_parser_seek_and_get_pic ( cursor_reference, buffer_size_fwd
                                         , buffer_size_bwd, n
                                         , pic_size_reference, false );
  }
  
  int
  dirac_parser_get_i_picture_skip_n ( uint8 const** cursor_reference
                                    , size_t        buffer_size_fwd
                                    , size_t        buffer_size_bwd
                                    , int*          n
                                    , size_t*       pic_size_reference )
  {
    return dirac_parser_seek_and_get_pic ( cursor_reference, buffer_size_fwd
                                         , buffer_size_bwd, n
                                         , pic_size_reference, true );  
  }
  
  int
  dirac_parser_get_previous_i_picture ( uint8 const** cursor_reference
                                      , size_t        buffer_size_fwd
                                      , size_t        buffer_size_bwd
                                      , size_t*       pic_size_reference )
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor_reference cannot be null"
                          , 0 != cursor_reference );
    DIRAC_MESSAGE_ASSERT  ( "pic_size_reference cannot be null"
                          , 0 != pic_size_reference );
    
    Scanner scanner ( *cursor_reference, buffer_size_fwd, buffer_size_bwd );
    while ( scanner.scanBackward() ) {
      ParseInfo pi ( scanner.cursor() );
      if ( pi.isIntra() ) {
        *pic_size_reference = pi.size();
        return DATA_OK;
      }
      // note: we don't need to move the cursor backwards
    }
    return NO_DATA;
  }
  


  int
  dirac_parser_move_cursor_fwd      ( uint8 const** cursor_reference
                                    , size_t        buffer_size_fwd )
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor_reference cannot be null"
                          , 0 != cursor_reference );
                        
    return moveCursorForward ( *cursor_reference, buffer_size_fwd );
  }
  
  
  
  int
  dirac_parser_is_access_unit       ( uint8 const*  cursor
                                    , size_t        buffer_size_fwd )
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor cannot be null", 0 != cursor );
    // check that cursor points to the start of an access unit and
    // that the entire access unit is contained in the passed buffer
    return ParseInfo::isParseInfo ( cursor, buffer_size_fwd )
        && ( !( buffer_size_fwd < ParseInfo ( cursor ).size() ) );
  }
  
  int
  dirac_parser_is_sequence_header   ( uint8 const*  cursor
                                    , size_t        buffer_size_fwd )
  {
    DIRAC_MESSAGE_ASSERT ( "cursor cannot be null", 0 != cursor );
    
    return dirac_parser_is_access_unit ( cursor, buffer_size_fwd )
        && ParseInfo ( cursor ).isSequenceHeader();
  }
  
  int
  dirac_parser_is_picture           ( uint8 const*  cursor
                                    , size_t        buffer_size_fwd )
  {
    DIRAC_MESSAGE_ASSERT ( "cursor cannot be null", 0 != cursor );
    
    return dirac_parser_is_access_unit ( cursor, buffer_size_fwd )
        && ParseInfo ( cursor ).isPicture();
  }
  
  int
  dirac_parser_is_i_picture         ( uint8 const*  cursor
                                    , size_t        buffer_size_fwd )
  {
    DIRAC_MESSAGE_ASSERT ( "cursor cannot be null", 0 != cursor );
    
    return dirac_parser_is_access_unit ( cursor, buffer_size_fwd )
        && ParseInfo ( cursor ).isIntra();
  }
  
  int
  dirac_parser_is_auxiliary_data      ( uint8 const*  cursor
                                      , size_t        buffer_size_fwd )
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor cannot be null", 0 != cursor );
    
    return dirac_parser_is_access_unit ( cursor, buffer_size_fwd )
        && ParseInfo ( cursor ).isAuxiliaryData();
  }
  
  int
  dirac_parser_is_padding_data        ( uint8 const*  cursor
                                      , size_t        buffer_size_fwd )
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor cannot be null", 0 != cursor );
    
    return dirac_parser_is_access_unit ( cursor, buffer_size_fwd )
       &&  ParseInfo ( cursor ).isPaddingData();
  }
  
  int
  dirac_parser_is_end_of_sequence     ( uint8 const*  cursor
                                      , size_t        buffer_size_fwd )
  {
    DIRAC_MESSAGE_ASSERT ( "cursor cannot be null", 0 != cursor );
    
    return dirac_parser_is_access_unit ( cursor, buffer_size_fwd )
        && ParseInfo ( cursor ).isEndOfSequence();
  }
  
  int
  dirac_parser_is_last_end_of_sequence ( uint8 const* cursor
                                       , size_t       buffer_size_fwd )
  {
    DIRAC_MESSAGE_ASSERT ( "cursor cannot be null", 0 != cursor );
    
    return dirac_parser_is_access_unit ( cursor, buffer_size_fwd )
      && ParseInfo ( cursor ).isEndOfSequence()
      && ParseInfo ( cursor ).next() == cursor
      && ParseInfo ( cursor ).size() == buffer_size_fwd;
  }
  
  int
  dirac_parser_get_picture_number   ( uint8 const*  cursor
                                    , size_t        size
                                    , uint32*       pic_number_reference )
  {
    DIRAC_MESSAGE_ASSERT  ( "cursor cannot be null", 0 != cursor );
    DIRAC_MESSAGE_ASSERT  ( "cursor must point to start of picture access unit"
                          , ParseInfo::isParseInfo ( cursor, size ) 
                        &&  ParseInfo ( cursor ).isPicture() );
    DIRAC_MESSAGE_ASSERT  ( "pic_number_reference cannot be null"
                          , 0 != pic_number_reference );
    
    *pic_number_reference = readFourBytes ( ParseInfo ( cursor ).payload() );
    return DATA_OK; // it never returns 0, it's assumed the client has checked
    // that we have a picture before invoking this function
  }
  
  int
  dirac_parser_get_version_major()
  {
    return DIRAC_PARSER_VERSION_MAJOR;
  }
  
  int
  dirac_parser_get_version_minor()
  {
    return DIRAC_PARSER_VERSION_MINOR;
  }
  
  int
  dirac_parser_get_revision()
  {
    return DIRAC_PARSER_REVISION;
  }
  
} /* parser_project */

} /* dirac */
