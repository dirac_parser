/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: buffer.cpp 67 2008-03-04 13:16:25Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <parser/buffer.hpp>

namespace parser = ::dirac::parser_project;
using namespace parser;

Buffer::Buffer ( const_iterator& cursor, size_type size )
  : begin_ ( cursor ), end_ ( cursor + size ), cursor_ ( cursor )
{
  checkValidity();
}

Buffer::Buffer ( const_iterator& cursor, size_type sizeFwd, size_type sizeBwd )
  : begin_ ( cursor - sizeBwd ), end_ ( cursor + sizeFwd ), cursor_ ( cursor )
{
  checkValidity();
}

Buffer::~Buffer()
{
  checkValidity();
}

void
Buffer::checkValidity() const
{
  DIRAC_MESSAGE_ASSERT  ( "begin cannot be null",     0 != begin_ );
  DIRAC_MESSAGE_ASSERT  ( "end cannot be null",       0 != end_ );
  DIRAC_MESSAGE_ASSERT  ( "cursor cannot be null",    0 != cursor_ );
  DIRAC_MESSAGE_ASSERT  ( "end cannot precede begin", !( end_ < begin_ ) );
  DIRAC_MESSAGE_ASSERT  ( "cursor must be included in buffer"
                        , !( begin_ > cursor_ ) && !( cursor_ > end_ ) );
}

Buffer::const_iterator
Buffer::begin() const
{
  checkValidity();
  return begin_;
}

Buffer::const_iterator
Buffer::end() const
{
  checkValidity();
  return end_;
}

Buffer::size_type
Buffer::size() const
{
  checkValidity();
  return ( end_ - begin_ );
}

bool
Buffer::empty() const
{
  checkValidity();
  return begin_ == end_;
}

Buffer::const_reverse_iterator
Buffer::rbegin() const
{
  checkValidity();
  return const_reverse_iterator ( end_ );
}

Buffer::const_reverse_iterator
Buffer::rend() const
{
  checkValidity();
  return const_reverse_iterator ( begin_ );
}

Buffer::CursorProxy const
Buffer::cursor() const
{
  checkValidity();
  return CursorProxy ( const_cast<const_iterator> ( cursor_ ) );
}

Buffer::CursorProxy
Buffer::cursor()
{
  checkValidity();
  return CursorProxy ( cursor_ );
}

