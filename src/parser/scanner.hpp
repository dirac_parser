/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: scanner.hpp 37 2008-02-07 09:18:30Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _SCANNER_HPP_
#define _SCANNER_HPP_

#include <common/dirac.h>
#include <parser/buffer.hpp>

#include <iterator>

namespace dirac
{
namespace parser_project
{
  /*  responsible for scanning the bitstream contained in the buffer
      forward and backward looking for a ParseInfo Header. */
  class Scanner
  {
  public:  // type definitions
    typedef uint8                                   value_type;
    typedef value_type const*                       const_iterator;
    typedef value_type                              const_reference;
    typedef ptrdiff_t                               difference_type;
    typedef uint32                                  size_type;
    typedef std::reverse_iterator<const_iterator>   const_reverse_iterator;
    
  public:
    Scanner ( const_iterator& cursor, size_type sizeFwd );
    Scanner ( const_iterator& cursor, size_type sizeFwd, size_type sizeBwd);
    ~Scanner();
    // compiler generated methods are ok

  public:  // main methods
    /*  scan the associated buffer forward for the next access unit
        it starts by looking for a parse info header and then it ensures
        that the entire access unit associated with that PIH is contained in
        the buffer.
        Returns true if it finds an access unit; the cursor of the associated
        buffer will point to the start of the access unit.
        Returns false if it cannot find a PIH or the access unit is not 
        completely contaned in the buffer associated with buffer_. In this case
        the cursor of the associated buffer is moved as far forward as
        required (ie, the end of the buffer - 3, if we couldn't find anything,
        or the beginning of the PIH if the associated access unit didn't fit
        in the buffer)   */
    bool            scanForward();
    /*  scan the associated buffer backwards for the previous access unit,
        if there is one.
        If the cursor of the associated buffer points to the start of an access
        unit this function will search for the previous PIH. Otherwise
        it will look for a PIH while scanning back and that will be considered
        the end of the desired access unit. So it will then look for a PIH
        further up the buffer (still scanning backward) which will mark
        the start of the desired access unit.
        Only a sequence header can be the first access unit in dirac stream;
        if it finds a PIH without predecessors it must be of type SEQHDR or
        else it will not be parsed successfully.
        Returns true if it finds the desired access unit and leave the cursor
        on the start of the access unit.
        Returns false if unsuccessful and moves the cursor backward as far
        as required.   */
    bool            scanBackward();

    /*  returns the position of the cursor of the underlying buffer  */
    const_iterator  cursor() const { return buffer_.cursor(); }
    /*  returns the size of the available buffer scanning forward */
    size_type       sizeFwd() const { return buffer_.end() - buffer_.cursor(); }
    /*  returns the size of the available buffer scanning backward  */
    size_type       sizeBwd() const { return buffer_.cursor() - buffer_.begin(); }
  
  private:  // utils
    bool        isBufferInSyncFwd();
    bool        isBufferInSyncBwd();
    
    // we use this limit while scanning forward out of synch
    // there is a risk that the header is not genuine or that the length field
    // is corrupted, in which case we may try to grow the buffer up to 4GB
    // MAX_AU_SIZE has been chosen by considering 1080P50 intra only - FIXME
    enum { MAX_AU_SIZE = 3750000 };
    
  private:  // members
    Buffer      buffer_;
  };
  
  
  
} /* parser_project */

} /* dirac */


#endif /* _SCANNER_HPP_ */
