/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: parser.h 40 2008-02-10 18:02:04Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _PARSER_H_
#define _PARSER_H_


#include <common/dirac.h>

#include <string.h>

#ifdef  __cplusplus

namespace dirac
{
namespace parser_project
{
extern "C" {

#endif

  /*! The parser interface expects the user to manage the buffer containing
      the bitstream to parse. The API accepts a reference to a cursor into
      the buffer, pointing to the current parsing position in the stream.
      The parser API will move the cursor along the buffer, looking for
      the required type of access unit. If the cursor is already pointing
      to the start of the desired access unit the API will not move
      the cursor forward.
      The API also offers a function to move the cursor forward by exactly
      one access unit; this function is to be used once the stream is in sync.
      In this case the client can invoke a "get" followed by a "move".
      All functions return 0 in case of failure and non-zero upon success.
      In case of failure the cursor will be moved as far forward as possible.
      Non-zero is only returned if the entire access unit is contained in
      the passed buffer.

      While parsing the stream backwards the cursor will be moved to the start
      of the first access unit which occurs entirely before the current position
      of the cursor.
      The API expects all pointers to be not NULL. Undefined behaviour
      will ensue if this is not the case.   */

  /*! scans the passed buffer from the cursor position forward for up to
      buffer_size_fwd bytes looking for a sequence header entirely contained
      in the buffer. If found, the cursor position is moved to the start
      of the sequence header while the length of the access unit is saved
      in seq_hdr_size_reference.
      Returns non-zero upon success, when the cursor and seq_hdr_size_reference
      are changed, zero upon failure, when the cursor is moved as much forward
      as possible in the buffer. seq_hdr_size_reference is not changed in this
      case.
      cursor_reference and seq_hdr_size_reference cannot be null    */
  int
  dirac_parser_get_sequence_header    ( uint8 const** cursor_reference
                                      , size_t        buffer_size_fwd
                                      , size_t*       seq_hdr_size_reference );

  /*! same as above but for the pictures  */
  int
  dirac_parser_get_next_picture       ( uint8 const** cursor_reference
                                      , size_t        buffer_size_fwd
                                      , size_t*       pic_size_reference );

  /*! same as above but for any type of access unit  */
  int
  dirac_parser_get_next_access_unit   ( uint8 const** cursor_reference
                                      , size_t        buffer_size_fwd
                                      , size_t*       acc_unit_size_reference );

  /*! same as above but only for i pictures */
  int
  dirac_parser_get_next_i_picture     ( uint8 const** cursor_reference
                                      , size_t        buffer_size_fwd
                                      , size_t*       pic_size_reference );

  /*! scans the passed buffer forward or backward looking for a picture.
      The size of the available buffer is given by the sum of buffer_size_fwd
      and buffer_size_bwd. If scanning forward, n==0 means get the next pic,
      n==1 means the next + 1, etc. Negative values equates to scanning the
      buffer backward, so n==-1 means fetching the first picture entirely
      contained in the buffer and ending before the current position of the
      cursor.
      The function will move the cursor while trying to find the required pic.
      It returns non-zero upon success, when the cursor will be moved
      to the start of the required pic and pic_size_reference carries the
      length of the pic. Returns zero upon failure, when the cursor is moved
      as far forward (or backward, for negative values of n) as possible
      while pic_size_reference is not modified.
      n is decreased according to the pics found by the function scanning
      cursor_reference, n and pic_size_reference cannot be null   */
  int
  dirac_parser_get_picture_skip_n     ( uint8 const** cursor_reference
                                      , size_t        buffer_size_fwd
                                      , size_t        buffer_size_bwd
                                      , int*          n
                                      , size_t*       pic_size_reference );

  /*! same as above but for intra pictures only   */
  int
  dirac_parser_get_i_picture_skip_n   ( uint8 const** cursor_reference
                                      , size_t        buffer_size_fwd
                                      , size_t        buffer_size_bwd
                                      , int*          n
                                      , size_t*       pic_size_reference );

  /*! cursor_reference is a reference to the current position of the cursor
      in the client-provided buffer. buffer_size_fwd is the available data
      in the buffer past the cursor, buffer_size_bwd is the available data
      before the cursor (to be used for scanning the buffer backwards).
      picture_size is a reference to the size of the i picture parsed by this
      function. It will be set to 0 if it cannot find an I picture   */
  int
  dirac_parser_get_previous_i_picture ( uint8 const** cursor_reference
                                      , size_t        buffer_size_fwd
                                      , size_t        buffer_size_bwd
                                      , size_t*       pic_size_reference );

  /*! move the cursor by one access unit - this function should be used once
      the stream is in sync, ie we have already parsed at least one
      access unit.
      The function expects the cursor to point to the start of an access unit
      and that the entire access unit is contained in the passed buffer.
      It returns 0 if it cannot move the cursor to the start of the next access
      unit (this includes if it is on the last access unit in the buffer),
      non zero upon success.
      Note that you don't need to move the cursor if you're scanning the stream
      backwards, since it is assumed that the user is looking for the previous
      access unit, which should end one byte before the position pointed by the
      passed cursor.
      The cursor cannot be null (same applies to its reference)
      buffer_size_fwd carries the number of available bytes in the buffer
      from the cursor's position (scanning forward).  */
  int
  dirac_parser_move_cursor_fwd        ( uint8 const** cursor_reference
                                      , size_t        buffer_size_fwd );

  /*! the following group of functions are self-explanatory: the functions
      return 0 if the passed buffer doesn't contain the expected type
      of dirac access unit, and non zero otherwise.
      Note that cursor must point to the start of the access unit;
      the entire access unit must be contained in the passed buffer for
      this function to return non-zero.
      The cursor cannot be null.     */
  int
  dirac_parser_is_access_unit         ( uint8 const*  cursor
                                      , size_t        buffer_size_fwd );
  int
  dirac_parser_is_sequence_header     ( uint8 const*  cursor
                                      , size_t        buffer_size_fwd );
  int
  dirac_parser_is_picture             ( uint8 const*  cursor
                                      , size_t        buffer_size_fwd );
  int
  dirac_parser_is_i_picture           ( uint8 const*  cursor
                                      , size_t        buffer_size_fwd );
  int
  dirac_parser_is_auxiliary_data      ( uint8 const*  cursor
                                      , size_t        buffer_size_fwd );
  int
  dirac_parser_is_padding_data        ( uint8 const*  cursor
                                      , size_t        buffer_size_fwd );
  int
  dirac_parser_is_end_of_sequence     ( uint8 const*  cursor
                                      , size_t        buffer_size_fwd );
  int
  dirac_parser_is_last_end_of_sequence
                                      ( uint8 const*  cursor
                                      , size_t        buffer_size_fwd );

  /*! to be used on access units containing pictures - this function expects
      the cursor to point to the start of an access unit and contain a picture.
      Failure to ensure that will result in undefined behaviour.
      It will check that there is enough data to decode the entire picture.
      It will return 0 if it cannot decode the picture number (eg, because
      the buffer doesn't contain an access unit or because the buffer is not
      long enough or doesn't start with an access unit) and pic_number will
      not be changed.
      It will return non zero if successful and will set *pic_number to
      the picture number.
      cursor cannot be null, cursor must point to start of picture access unit
      or else undefined behaviour  */
  int
  dirac_parser_get_picture_number     ( uint8 const*  cursor
                                      , size_t        size
                                      , uint32*       pic_number_reference );

  /*! current version of this API */
  int
  dirac_parser_get_version_major();
  int
  dirac_parser_get_version_minor();
  int
  dirac_parser_get_revision();

#ifdef __cplusplus

} /* extern "C" */

} /* parser_project */

} /* dirac */


/* we'll use this alias for the Parser module */
namespace parser = ::dirac::parser_project;

#endif

#endif /* _PARSER_H_ */
