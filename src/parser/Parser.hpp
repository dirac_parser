/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: Parser.hpp 67 2008-03-04 13:16:25Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _PARSER_HPP_
#define _PARSER_HPP_

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <common/dirac.h>
#include <common/util.hpp>
#include <parser/StreamReader.hpp>
#include <parser/parser.h>
#include <parser/AccessUnit.hpp>

#include <iterator>
#include <memory>


namespace dirac
{
namespace parser_project
{
  
  //! C++ wrapper for the parser API - aims to offer the same functionality
  class Parser {
    typedef int (*parser_getter) ( uint8 const**, size_t, size_t*);
    
  public: 
    explicit Parser ( std::auto_ptr<StreamReader> str ) : str_ ( str ) 
    {
      checkValidity();
    }
    ~Parser() 
    {
      checkValidity();
    }
  private:  // compiler generated methods are not allowed
    Parser ( Parser const& );
    Parser& operator= ( Parser const& );
    
  public: 
    //! retrieves the next sequence header in the associated stream
    //! au will contain a copy of the sequence header, if found
    //! the cursor will not be moved past the beginning of the seqhdr, if found
    bool              getNextSequenceHeader ( AccessUnit& au)
    {
      checkValidity();
      return getAccessUnitFwd ( dirac_parser_get_sequence_header, au );
    }
    
    //! retrieves the next picture in the stream and copies content in au
    //! the cursor will point to the start of the pic, if found
    bool              getNextPicture ( AccessUnit& au )
    {
      checkValidity();
      return getAccessUnitFwd ( dirac_parser_get_next_picture, au );
    }
    
    //! retrieves the next i picture in the stream and copies content in au
    //! the cursor will point to the start of the i pic, if found
    bool              getNextIPicture ( AccessUnit& au )
    {
      checkValidity();
      return getAccessUnitFwd ( dirac_parser_get_next_i_picture, au );
    }

    //! retrieves the next access unit and copies content in au
    //! the cursor will point to the start of the access unit, if found
    bool              getNextAccessUnit ( AccessUnit& au )
    {
      checkValidity();
      return getAccessUnitFwd ( dirac_parser_get_next_access_unit, au );
    }

    //! Retrieves I pics forward or backward in the bitstream and copies
    //! content in au, if found
    //! the cursor will point to the start of the requested i pic, if found
    bool              getNextIPictureSkipN ( AccessUnit& au, int n )
    {
      checkValidity();
      size_t au_size = 0;
      int number = n;
      while ( true ) {
        if ( !dirac_parser_get_i_picture_skip_n ( &str_->gptr()
                                                , str_->sizeFwd()
                                                , str_->sizeBwd()
                                                , &number
                                                , &au_size ) ) {
          if ( 0 > n ) {
            if ( !str_->addDataBwd() ) return false;
            else continue;
          }
          else {
            if ( !str_->addDataFwd() ) return false;
            else continue;
          }
        }
        else {
          au.setp ( str_->gptr(), au_size );
          return true;
        }
      }
    }
    
    //! Retrieves I pics bwd in the bitstream and copies content in au
    //! the cursor will point to the start of the requested i pic, if found
    bool              getPreviousIPicture ( AccessUnit& au )
    {
      checkValidity();
      size_t au_size = 0;
      while ( true ) {
        if ( !dirac_parser_get_previous_i_picture ( &str_->gptr()
                                                  , str_->sizeFwd()
                                                  , str_->sizeBwd()
                                                  , &au_size ) ) {
          if ( !str_->addDataBwd() ) return false;
          else continue;
        }
        else {
          au.setp ( str_->gptr(), au_size );
          return true;
        }
      }
    }
    
    //! Determines whether the cursor is pointing to the start of a dirac AU
    bool              isAccessUnit() const
    {
      checkValidity();
      return dirac_parser_is_access_unit ( str_->gptr(), str_->sizeFwd() );
    }
    
    //! Determines whether the cursor is pointing to the start of a dirac SeqHdr
    bool              isSequenceHeader() const
    {
      checkValidity();
      return dirac_parser_is_sequence_header ( str_->gptr(), str_->sizeFwd() );
    }
    
    //! Determines whether the cursor is pointing to the start of a picture
    bool              isPicture() const
    {
      checkValidity();
      return dirac_parser_is_picture ( str_->gptr(), str_->sizeFwd() );
    }
    
    //! Determines whether the cursor is pointing to the start of an intra pic
    bool              isIPicture() const
    {
      checkValidity();
      return dirac_parser_is_i_picture ( str_->gptr(), str_->sizeFwd() );
    }
    
    //! Determines whether the cursor is pointing to the start of aux data
    bool              isAuxiliaryData() const
    {
      checkValidity();
      return dirac_parser_is_auxiliary_data ( str_->gptr(), str_->sizeFwd() );
    }
    
    //! Determines whether the cursor is pointing to the start of padding data
    bool              isPaddingData() const
    {
      checkValidity();
      return dirac_parser_is_padding_data ( str_->gptr(), str_->sizeFwd() );
    }
    
    //! Determines whether the cursor is pointing to the start of the end of seq
    bool              isEndOfSequence() const
    {
      checkValidity();
      return dirac_parser_is_end_of_sequence ( str_->gptr(), str_->sizeFwd() );
    }
    
    //! Determines whether the cursor if pointing to the start of the last
    //! end of sequence of the stream
    bool              isLastEndOfSequence() const
    {
      checkValidity();
      return dirac_parser_is_last_end_of_sequence ( str_->gptr(), str_->sizeFwd() );
    }
    
    //! Retrieves pic number, if cursor points to start of pic
    bool              getPictureNumber ( uint32& n ) const
    {
      checkValidity();
      return dirac_parser_get_picture_number  ( str_->gptr()
                                              , str_->sizeFwd()
                                              , &n );
    }
    
    //! Move cursor forward by one access unit - Note that the user must
    //! ensure that the cursor points to start of AU or else undefined behaviour
    bool              moveCursorFwd()
    {
      checkValidity();
      while ( true ) {
        if ( !dirac_parser_move_cursor_fwd ( &str_->gptr(), str_->sizeFwd() ) ) {
          if ( !str_->addDataFwd() ) return false;
          else continue;
        }
        else return true;
      }
    }
    
    //! Rewind the cursor to the beginning of the stream, if possible
    //! otherwise it has no effect
    bool              rewind()
    {
      checkValidity();
      return str_->rewind();
    }
    
    //! Allows to swap two Parser objects, guaranteed not to throw
    void              swap ( Parser& rhs )
    {
      checkValidity();
      std::auto_ptr<StreamReader> tmp ( str_.release() );
      str_ = rhs.str_;
      rhs.str_ = tmp;
    }
    
    //! Returns the major version number of this API
    int               getVersionMajor() const
    {
      return dirac_parser_get_version_major();
    }
    
    //! Returns the minor version number of this API
    int               getVersionMinor() const
    {
      return dirac_parser_get_version_minor();
    }
    
    //! Returns the revision number of this API
    int               getRevision() const
    {
      return dirac_parser_get_revision();
    }
    
  private:  // helper methods
    //! helper class used to implement getter methods
    bool              getAccessUnitFwd ( parser_getter getter, AccessUnit& au)
    {
      size_t au_size = 0;
      while ( true ) {
        if ( !getter ( &str_->gptr(), str_->sizeFwd(), &au_size ) ) {
          if ( !str_->addDataFwd() ) return false;
          else continue;
        }
        else {
          au.setp ( str_->gptr(), au_size );
          return true;
        }
      }
    }
    
    void              checkValidity() const
    {
      DIRAC_MESSAGE_ASSERT ( "stream reader cannot be null", 0 != str_.get() );
    }
    
  private:  // members
    //! stream buffer used to hold the dirac bitstream
    std::auto_ptr<StreamReader>        str_;
  };
} /* parser_project */

} /* dirac */


#endif /* _PARSER_HPP_ */
