/* ***** BEGIN LICENSE BLOCK *****
*
* $Id: buffer.hpp 37 2008-02-07 09:18:30Z andrea $
*
* Version: MPL 1.1/GPL 2.0/LGPL 2.1
*
* The contents of this file are subject to the Mozilla Public License
* Version 1.1 (the "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
* http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
* the specific language governing rights and limitations under the License.
*
* The Original Code is BBC Research code.
*
* The Initial Developer of the Original Code is the British Broadcasting
* Corporation.
* Portions created by the Initial Developer are Copyright (C) 2007.
* All Rights Reserved.
*
* Contributor(s): Andrea Gabriellini (Original Author)
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU General Public License Version 2 (the "GPL"), or the GNU Lesser
* Public License Version 2.1 (the "LGPL"), in which case the provisions of
* the GPL or the LGPL are applicable instead of those above. If you wish to
* allow use of your version of this file only under the terms of the either
* the GPL or LGPL and not to allow others to use your version of this file
* under the MPL, indicate your decision by deleting the provisions above
* and replace them with the notice and other provisions required by the GPL
* or LGPL. If you do not delete the provisions above, a recipient may use
* your version of this file under the terms of any one of the MPL, the GPL
* or the LGPL.
* ***** END LICENSE BLOCK ***** */

#ifndef _BUFFER_HPP_
#define _BUFFER_HPP_

#include <common/dirac.h>

#include <iterator>

namespace dirac
{
namespace parser_project
{
  
  class Buffer 
  {
  public:  // some type definitions
    typedef uint8 const*                            const_pointer;
    typedef uint8 const*                            const_iterator;
    typedef uint32                                  size_type;
    typedef std::reverse_iterator<const_iterator>   const_reverse_iterator;
    
  public: // contructions
    Buffer ( const_iterator& cursor, size_type size );
    Buffer ( const_iterator& cursor, size_type sizeFwd, size_type sizeBwd );
    ~Buffer();
    
    // compiler generated methods are OK
  
  public: // helper class
    //! Proxy class used to discriminate between lvalue and rvalue use of cursor
    //! I decided to use a proxy because I can control its use
    class CursorProxy
    {
      public:  // type definitions
        typedef Buffer::size_type                   size_type;
        typedef Buffer::const_iterator              const_iterator;
        
      public: // construction
        CursorProxy ( const_iterator& cursor )
          : cursor_ ( cursor ) {}
        ~CursorProxy() {}
        // compiler generated methods are OK
        
      public:
        // lvalue uses
        CursorProxy& operator= ( const_iterator c ) { cursor_ = c; return *this; }
        CursorProxy& operator++() { ++cursor_; return *this; }
        
        // rvalue uses
        operator const_iterator() const { return cursor_; }
      
      private:  // members
        const_iterator& cursor_;
    };
    
  public: // main methods
    const_iterator              begin()         const;
    const_iterator              end()           const;
    size_type                   size()          const;
    bool                        empty()         const;
    
    const_reverse_iterator      rbegin()        const;
    const_reverse_iterator      rend()          const;
    
    CursorProxy const           cursor()        const;  //  rvalue
    CursorProxy                 cursor();               // lvalue
    
  private:  // helper methods
    void                        checkValidity() const;
    
  private:  // members
    const_iterator  begin_;
    const_iterator  end_;
    const_iterator& cursor_;
  };
  
} /* parser_project */

} /* dirac */


#endif /* _BUFFER_HPP_ */
